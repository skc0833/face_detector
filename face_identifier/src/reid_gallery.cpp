// Copyright (C) 2018-2019 Intel Corporation
// SPDX-License-Identifier: Apache-2.0
//

#include <pch.h>
#include "face_reid.hpp"
#include "tracker.hpp"

#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <limits>

#include <opencv2/opencv.hpp>

namespace {
    float ComputeReidDistance(const cv::Mat& descr1, const cv::Mat& descr2) {
        float xy = static_cast<float>(descr1.dot(descr2));
        float xx = static_cast<float>(descr1.dot(descr1));
        float yy = static_cast<float>(descr2.dot(descr2));
        float norm = sqrt(xx * yy) + 1e-6f;
        return 1.0f - xy / norm;
    }

    bool file_exists(const std::string& name) {
        std::ifstream f(name.c_str());
        return f.good();
    }

    inline char separator() {
        #ifdef _WIN32
        return '\\';
        #else
        return '/';
        #endif
    }

    std::string folder_name(const std::string& path) {
        size_t found_pos;
        found_pos = path.find_last_of(separator());
        if (found_pos != std::string::npos)
            return path.substr(0, found_pos);
        return std::string(".") + separator();
    }

}  // namespace

const char EmbeddingsGallery::unknown_label[] = "Unknown";
const int EmbeddingsGallery::unknown_id = TrackedObject::UNKNOWN_LABEL_IDX;

RegistrationStatus EmbeddingsGallery::RegisterIdentity(const std::string& identity_label,
                                                       const cv::Mat& image,
                                                       int min_size_fr, bool crop_gallery,
                                                       detection::FaceDetection& detector,
                                                       const VectorCNN& landmarks_det,
                                                       const VectorCNN& image_reid,
                                                       cv::Mat& embedding) {
    cv::Mat target = image;
    if (crop_gallery) {
      detector.enqueue(image);
      detector.submitRequest();
      detector.wait();
      detector.fetchResults();
      detection::DetectedObjects faces = detector.results;
      if (faces.size() == 0) {
        return RegistrationStatus::FAILURE_NOT_DETECTED;
      }
      cv::Mat face_roi = image(faces[0].rect);
      target = face_roi;
    }
    if ((target.rows < min_size_fr) && (target.cols < min_size_fr)) {
      return RegistrationStatus::FAILURE_LOW_QUALITY;
    }
    cv::Mat landmarks;
    landmarks_det.Compute(target, &landmarks, cv::Size(2, 5));
    std::vector<cv::Mat> images = {target};
    std::vector<cv::Mat> landmarks_vec = {landmarks};
    AlignFaces(&images, &landmarks_vec);
    image_reid.Compute(images[0], &embedding);
    return RegistrationStatus::SUCCESS;
}

#if skc_test
#define skc_test_xml    1
#endif

#if skc_test_xml
using namespace std;
using namespace cv;
// C:\Users\skc08\Documents\Intel\OpenVINO\omz_demos_build\smart_classroom_demo\skc_test_gallery.xml
static const string skc_test_gallery_path = "../_bin/skc_test_gallery.xml";

static void skc_load_from_xml(std::vector<int>& idx_to_id, std::vector<GalleryObject>& identities) {
    if (!file_exists(skc_test_gallery_path)) {
        CV_Assert(false);
    }
    // 기존에 파일로 저장했던 embedding 값들을 로딩
    FileStorage fs(skc_test_gallery_path, FileStorage::READ);
    FileNode features = fs["features"];
    FileNodeIterator it = features.begin(), it_end = features.end();
    int idx = 0;


    // iterate through a sequence using FileNodeIterator
    for (; it != it_end; ++it, idx++)
    {
        //cout << "feature #" << idx << ": ";

        int id = (int)(*it)["id"];
        string label = (string)(*it)["label"];
        Mat emb; (*it)["emb"] >> emb;
#if skc_test_show_top3
        string path = (string)(*it)["path"];
#else
//#error 111
#endif

        //cout << "id=" << id << ", label=" << label /*<< ", emb=" << emb*/ << ", path=" << path << endl;

        std::vector<cv::Mat> embeddings;
        embeddings.push_back(emb);
        idx_to_id.push_back(id);
#if skc_test_show_top3
        identities.emplace_back(embeddings, label, id, path);
#else
        identities.emplace_back(embeddings, label, id);
#endif
    }
    fs.release();
}
#endif

EmbeddingsGallery::EmbeddingsGallery(const std::string& ids_list,
                                     double threshold, int min_size_fr,
                                     bool crop_gallery, detection::FaceDetection& detector,
                                     const VectorCNN& landmarks_det,
                                     const VectorCNN& image_reid)
    : reid_threshold(threshold) {
    if (ids_list.empty()) {
        return;
    }

    if (!landmarks_det.Enabled() || !image_reid.Enabled()) {
        return;
    }

#if skc_test_xml
    if (file_exists(skc_test_gallery_path)) {
        skc_load_from_xml(idx_to_id, identities);
        return;
    }

    // 아직 skc_test_gallery_path 파일이 없으면 embedding 값들을 파일로 저장
    cv::FileStorage skc_write_fs(skc_test_gallery_path, cv::FileStorage::WRITE);
    skc_write_fs << "features" << "[";
#endif
    cv::FileStorage fs(ids_list, cv::FileStorage::Mode::READ);
    cv::FileNode fn = fs.root();
    int total_images = 0;
    int id = 0;
    for (cv::FileNodeIterator fit = fn.begin(); fit != fn.end(); ++fit) {
        cv::FileNode item = *fit;
        std::string label = item.name();
        std::vector<cv::Mat> embeddings;
        CV_Assert(item.size() == 1);

        for (size_t i = 0; i < item.size(); i++) {
            std::string path;
            if (file_exists(item[i].string())) {
                path = item[i].string();
            } else {
                path = folder_name(ids_list) + separator() + item[i].string();
            }

            cv::Mat image = cv::imread(path);
            if (image.empty()) {
                std::cout << "@@skc ------> skip not found file " << path << std::endl;
                continue;
            }
            cv::Mat emb;
            RegistrationStatus status = RegisterIdentity(label, image, min_size_fr, crop_gallery,  detector, landmarks_det, image_reid, emb);
#if CU_NIPA
            CV_Assert(status == RegistrationStatus::SUCCESS); //@@skc_test
#endif
            if (status == RegistrationStatus::SUCCESS) {
                embeddings.push_back(emb);
                idx_to_id.push_back(id);
                total_images++;
#if skc_test_xml
#if skc_test_show_top3
                identities.emplace_back(embeddings, label, id, path);
                skc_write_fs << "{:" << "id" << id << "label" << label << "emb" << emb << "path" << path << "}";
#else
                identities.emplace_back(embeddings, label, id);
                skc_write_fs << "{:" << "id" << id << "label" << label << "emb" << emb << "}";
#endif
#else   // org
                identities.emplace_back(embeddings, label, id);
#endif
                ++id;
            }
        }
    }
#if skc_test_xml
    skc_write_fs << "]";
    skc_write_fs.release();

    // check saved ok
    std::vector<int> idx_to_id2;
    std::vector<GalleryObject> identities2;
    skc_load_from_xml(idx_to_id2, identities2);
    for (int i = 0; i < idx_to_id2.size(); ++i) {
        if (idx_to_id2[i] != idx_to_id[i]) {
            CV_Assert(false);
        }
    }
    for (int i = 0; i < identities2.size(); ++i) {
        if (identities2[i].id != identities[i].id || identities2[i].label != identities[i].label) {
            CV_Assert(false);
        }
        for (int k = 0; k < identities2[i].embeddings.size(); ++k) {
            cv::Mat diff = identities2[i].embeddings[k] != identities[i].embeddings[k];
            // Equal if no elements disagree
            bool eq = cv::countNonZero(diff) == 0;
            if (!eq) {
                cout << "@@@ diff i=" << i << endl;
                cout << "org embeddings=" << identities[i].embeddings[k] << endl;
                cout << "embeddings2=" << identities2[i].embeddings[k] << endl;
                CV_Assert(false);
            }
        }
    }
#endif
}

#if CU_NIPA
std::vector<EmbeddingsGallery::MatchInfo> 
EmbeddingsGallery::GetIDsByEmbeddingsEx(const std::vector<cv::Mat>& embeddings, bool useKuhnMunkres) const {
    std::vector<MatchInfo> output_ids;

    if (embeddings.empty() || idx_to_id.empty()) {
        return output_ids; // 발생중임???
    }

    cv::Mat distances(static_cast<int>(embeddings.size()), static_cast<int>(idx_to_id.size()), CV_32F);

    for (int i = 0; i < distances.rows; i++) { // 전달받은 embeddings 각각에 대해
        int k = 0;
        for (size_t j = 0; j < identities.size(); j++) { // 모든 갤러리 객체와의 거리를 distances 에 저장
            for (const auto& reference_emb : identities[j].embeddings) {
                distances.at<float>(i, k) = ComputeReidDistance(embeddings[i], reference_emb);
                k++;
            }
        }
    }

    if (useKuhnMunkres) {
        KuhnMunkres matcher;
        auto matched_idx = matcher.Solve(distances);
        //std::vector<int> output_ids;
        for (auto col_idx : matched_idx) {
            auto distance = distances.at<float>(output_ids.size(), col_idx);
            if (distances.at<float>(output_ids.size(), col_idx) > reid_threshold)
                output_ids.push_back({ idx_to_id[col_idx], distance }); // unknown_id
            else
                output_ids.push_back({ idx_to_id[col_idx], distance });
        }
        return output_ids;
    } else {
        for (int y = 0; y < distances.rows; y++) { // 전달받은 embeddings 각각에 대해
            cv::Mat r = distances.row(y);
            auto min0 = std::min_element(r.begin<float>(), r.end<float>(), [](const auto& a, const auto& b) {
                return a < b;
            });
            size_t col_idx = min0 - r.begin<float>();
            float distance = *min0;
            output_ids.push_back({ idx_to_id[col_idx], distance });
        }
        return output_ids;
    }
}
#endif

std::vector<int> EmbeddingsGallery::GetIDsByEmbeddings(const std::vector<cv::Mat>& embeddings) const {
    if (embeddings.empty() || idx_to_id.empty())
        return std::vector<int>();

    cv::Mat distances(static_cast<int>(embeddings.size()), static_cast<int>(idx_to_id.size()), CV_32F);

    for (int i = 0; i < distances.rows; i++) {
        int k = 0;
        for (size_t j = 0; j < identities.size(); j++) {
            for (const auto& reference_emb : identities[j].embeddings) {
                distances.at<float>(i, k) = ComputeReidDistance(embeddings[i], reference_emb);
                k++;
            }
        }
    }
#if skc_test_show_top3
    distances.copyTo(m_distances);
#endif

#if skc_test_show_top3
    //KuhnMunkres matcher;
    //auto matched_idx = matcher.Solve(distances);
    std::vector<int> output_ids;
    //for (auto col_idx : matched_idx) {
    //  if (false && distances.at<float>(output_ids.size(), col_idx) > reid_threshold) //@@skc test
    //    output_ids.push_back(unknown_id);
    //  else
    //    output_ids.push_back(idx_to_id[col_idx]);
    //}

    std::vector<int> output_ids_topX;

    if (distances.rows == 1) {
      // get top 5
      const int top_k = 3;

      //std::sort(distances.begin<float>(), distances.end<float>());
      for (int i = 0; i < top_k; ++i) {
        std::cout << "@@skc top " << i << " --------------------------------" << std::endl;
        for (auto it = distances.begin<float>(); it != distances.end<float>(); ++it) {
          //std::cout << *it << ", " << std::endl;
        }
        //@@skc 원래 초기에는 auto& min0 를 사용했었으나, 별도 프로젝트 구성 후 컴파일 에러로 auto min0 로 변경함
        auto min0 = std::min_element(distances.begin<float>(), distances.end<float>(), [](const auto& a, const auto& b) {
          return a < b;
        });
        size_t col_idx = min0 - distances.begin<float>();
        float distance = *min0;
        if (distance > reid_threshold) {
          //col_idx = unknown_id;
        }
        std::cout << "search min -> [" << i << "] col=" << col_idx << ", distance=" << distance << std::endl;
        output_ids_topX.emplace_back(col_idx);
        *min0 = std::numeric_limits<float>::max(); // 처리된 항목은 다음번 계산에서 빼기 위해 max() 로 설정함
      }
      // check result is same
      //if (output_ids_topX.front() != output_ids.front()) {
      //  std::cerr << "@@skc error!!! not equal" << std::endl;
      //}
      return output_ids_topX;
    }
    else {
      return output_ids;
    }
#else
    KuhnMunkres matcher;
    auto matched_idx = matcher.Solve(distances);
    std::vector<int> output_ids;
    for (auto col_idx : matched_idx) {
      //if (false && distances.at<float>(output_ids.size(), col_idx) > reid_threshold) //@@skc test
      if (distances.at<float>(output_ids.size(), col_idx) > reid_threshold)
        output_ids.push_back(unknown_id);
      else
        output_ids.push_back(idx_to_id[col_idx]);
    }
    return output_ids;
#endif
}

std::string EmbeddingsGallery::GetLabelByID(int id) const {
    if (id >= 0 && id < static_cast<int>(identities.size()))
        return identities[id].label;
    else
        return unknown_label;
}

#if skc_test_show_top3
std::string EmbeddingsGallery::GetPathByID(int id) const {
    if (id >= 0 && id < static_cast<int>(identities.size()))
        return identities[id].path;
    else
        return "";
}
#endif

size_t EmbeddingsGallery::size() const {
    return identities.size();
}

std::vector<std::string> EmbeddingsGallery::GetIDToLabelMap() const  {
    std::vector<std::string> map;
    map.reserve(identities.size());
    for (const auto& item : identities)  {
        map.emplace_back(item.label);
    }
    return map;
}

bool EmbeddingsGallery::LabelExists(const std::string& label) const {
    return identities.end() != std::find_if(identities.begin(), identities.end(),
                                        [label](const GalleryObject& o){return o.label == label;});
}
