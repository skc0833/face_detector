﻿// Copyright (C) 2018-2019 Intel Corporation
// SPDX-License-Identifier: Apache-2.0
//

#include <pch.h>
#include <chrono>  // NOLINT

#include <gflags/gflags.h>
#include <samples/ocv_common.hpp>
#include <samples/slog.hpp>
#include <ext_list.hpp>
#include <string>
#include <memory>
#include <limits>
#include <vector>
#include <deque>
#include <map>
#include <set>
#include <algorithm>
#include <utility>
#include <ie_iextension.h>

#include "actions.hpp"
#include "action_detector.hpp"
#include "cnn.hpp"
#include "detector.hpp"
#include "face_reid.hpp"
#include "tracker.hpp"
#include "image_grabber.hpp"
#include "logger.hpp"
#include "smart_classroom_demo.hpp"

#if CU_NIPA

#if CU_MULTI_CAM
#include "../common/graph.hpp"
#include "../common/output.hpp"
#include "../common/samples/args_helper.hpp"
#endif

// [101008][skc] smart classroom 원본 소스에서 FLAGS_i 에 rtsp 를 설정할 경우, detector 등을 켜면 
// 프레임 재생도중 opencv 에서 에러가 발생중이므로(별도 worker thread 필요해보임)
// rtsp 재생시에는 CU_TEST_DRAW_FRAME_ONLY 를 켜줘야 한다.
//#define CU_TEST_DRAW_FRAME_ONLY   // CU_PERF: CU_MULTI_CAM 도 끄고, 이걸로 테스트시 60 fps 이상(classroom.mp4)

#define CU_ENABLE_PERSON_DETECTOR   // CU_PERF: PERSON_DETECTOR 만 켤시 22 fps(classroom.mp4)
#define CU_DISABLE_FACE_DETECTOR  // CU_PERF: PERSON_DETECTOR, FACE_DETECTOR -> 9 fps(classroom.mp4)
#define CU_DISABLE_ACTION_DETECTOR  // [skc] CU_ENABLE_PERSON_DETECTOR 와 택1(둘다 켜져도 상관은 없을듯함)

// CU_PERF: CU_MULTI_CAM, CU_MULTI_CAM_FRAME_ONLY, 1 ch -> 60 fps, 4 ch -> 25 fps(대각선 영상끼리만 싱크가 비슷함, 나머지는 몇초 delay 발생중)
// 여기에 자체 PERSON_DETECTOR 까지 추가할 경우, 1 ch -> 23 fps, 4 ch -> 1 fps

#ifdef CU_TEST_DRAW_FRAME_ONLY
#undef  CU_ENABLE_PERSON_DETECTOR
#define CU_DISABLE_FACE_DETECTOR
#define CU_DISABLE_ACTION_DETECTOR
#endif

#ifdef CU_ENABLE_PERSON_DETECTOR
#include "person/person_detector.hpp"
#include "person/person_tracker.hpp"
#include "person/person_descriptor.hpp"
#include "person/person_distance.hpp"
#endif

/*
// release mode
-i $(SolutionDir)model\classroom.mp4 
-m_act $(SolutionDir)model\F32\person-detection-action-recognition-0005.xml 
-m_fd $(SolutionDir)model\F32\face-detection-adas-0001.xml 
-m_reid $(SolutionDir)model\F32\face-reidentification-retail-0095.xml 
-m_lm $(SolutionDir)model\F32\landmarks-regression-retail-0009.xml 
-fg $(SolutionDir)model\faces_gallery_60.json 
-t_reid 0.4 -d_fd CPU -d_lm CPU -d_reid CPU 
-min_size_fr 32

//-m_person_det $(SolutionDir)model\F32\person_detection\person-detection-retail-0013.xml
//-m_person_reid $(SolutionDir)model\F32\person_detection\person-reidentification-retail-0031.xml

// 1) 원본
[ INFO ] Mean FPS: 6.25795
[ INFO ] Frames processed: 984
Performance counts for action detector
Total time: 156903   microseconds
Full device name: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz

Performance counts for face detector
Total time: 97287    microseconds
Full device name: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz

Performance counts for D:\work\cubox\face\git\face_detector\model\F32\face-reidentification-retail-0095.xml
Total time: 76762    microseconds
Full device name: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz

Performance counts for D:\work\cubox\face\git\face_detector\model\F32\landmarks-regression-retail-0009.xml
Total time: 2319     microseconds
Full device name: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz

[ INFO ] Final ID->events mapping
Person: person 2
   - sitting: from 0 to 984 frames
Person: person 3
   - sitting: from 0 to 99 frames
   - standing: from 99 to 470 frames
   - sitting: from 470 to 984 frames
Person: person 1
   - Unknown: from 0 to 984 frames
Person: person 4
   - Unknown: from 0 to 984 frames
[ INFO ] Final per-frame ID->action mapping
[ INFO ] Execution successful

// 2) action detector 를 끌 경우(CU_DISABLE_ACTION_DETECTOR)
[ INFO ] Mean FPS: 10.9588
[ INFO ] Frames processed: 984
Performance counts for face detector
Total time: 57789    microseconds
Full device name: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz

Performance counts for D:\work\cubox\face\git\face_detector\_bin\Release/../../model/F32/face-reidentification-retail-0095.xml
Total time: 60409    microseconds
Full device name: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz

Performance counts for D:\work\cubox\face\git\face_detector\_bin\Release/../../model/F32/landmarks-regression-retail-0009.xml
Total time: 1611     microseconds
Full device name: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz

[ INFO ] Final ID->events mapping
[ INFO ] Final per-frame ID->action mapping
[ INFO ] Execution successful

// 3) action detector 끄고 person detector 켠 경우(CU_ENABLE_PERSON_DETECTOR)
[ INFO ] Mean FPS: 7.12641
[ INFO ] Frames processed: 984
Performance counts for face detector
Total time: 64681    microseconds
Full device name: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz

Performance counts for D:\work\cubox\face\git\face_detector\_bin\Release/../../model/F32/face-reidentification-retail-0095.xml
Total time: 39157    microseconds
Full device name: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz

Performance counts for D:\work\cubox\face\git\face_detector\_bin\Release/../../model/F32/landmarks-regression-retail-0009.xml
Total time: 960      microseconds
Full device name: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz

// 아무 처리도 안할 경우, 40fps
// person detect 만 할 경우, 16fps

*/

static std::string cu_convert_path(std::string path) {
    static std::string exe_path;
    if (exe_path.size() == 0) {
        char strExePath[MAX_PATH] = { 0 };
        ::GetModuleFileName(0, strExePath, MAX_PATH);
        exe_path = strExePath;
    }
    auto pos = exe_path.rfind('\\');
    if (pos == std::string::npos)
        assert(false);
    std::string result = exe_path.substr(0, pos) + path;
    return result;
}

static void cu_set_param() {
#ifdef CU_TEST_DRAW_FRAME_ONLY
    FLAGS_i         = "rtsp://admin:cubox2019!1@172.16.160.231:554/Straming/Channels/101"; // 231 ~ 234
#endif
    //FLAGS_i         = cu_convert_path("/../../model/classroom.mp4");
    //FLAGS_i         = cu_convert_path("/../../model/store-aisle-detection.mp4");
    //FLAGS_i         = "cam";
    FLAGS_m_act     = cu_convert_path("/../../model/F32/person-detection-action-recognition-0005.xml");
    FLAGS_m_fd      = cu_convert_path("/../../model/F32/face-detection-adas-0001.xml");
    FLAGS_m_reid    = cu_convert_path("/../../model/F32/face-reidentification-retail-0095.xml");
    FLAGS_m_lm      = cu_convert_path("/../../model/F32/landmarks-regression-retail-0009.xml");
    FLAGS_fg        = cu_convert_path("/../../model/faces_gallery_60.json");
    FLAGS_t_reid    = 0.4;
    FLAGS_d_fd      = "CPU";
    FLAGS_d_lm      = "CPU";
    FLAGS_d_reid    = "CPU";
    FLAGS_min_size_fr = 32;
    FLAGS_pc        = true;
    //FLAGS_out_v     = cu_convert_path("/../../cu_debug.avi");

    ActionsType test_actions_type = STUDENT;

#ifdef CU_ENABLE_PERSON_DETECTOR
    FLAGS_m_person_det = cu_convert_path("/../../model/F32/person_detection/person-detection-retail-0013.xml");
    FLAGS_m_person_reid = cu_convert_path("/../../model/F32/person_detection/person-reidentification-retail-0031.xml");
    //FLAGS_m_person_reid = "";
#endif

#if CU_MULTI_CAM
    FLAGS_show_stats = true;
    FLAGS_nc = 4;
    FLAGS_bs = 1;
    //FLAGS_i = "rtsp ://admin:cubox2019!1@172.16.160.231:554/Straming/Channels/101"; //@@skc 불필요하지 않나???
    FLAGS_m = cu_convert_path("/../../model/F32/person_detection/person-detection-retail-0013.xml"); // 위 모델과 동일하게 맞춤
    FLAGS_n_ir = 5 * 2; // the number of infer requests(5 개와 별차이 없어보임)
#endif
}

#ifdef CU_ENABLE_PERSON_DETECTOR
std::unique_ptr<PedestrianTracker>
CreatePedestrianTracker(const std::string& reid_model,
    const std::string& reid_weights,
    const InferenceEngine::Core & ie,
    const std::string & deviceName,
    bool should_keep_tracking_info) {
    Person_TrackerParams params;

    if (should_keep_tracking_info) {
        params.drop_forgotten_tracks = false;
        params.max_num_objects_in_track = -1;
    }

    std::unique_ptr<PedestrianTracker> tracker(new PedestrianTracker(params));

    // Load reid-model.
    std::shared_ptr<IImageDescriptor> descriptor_fast =
        std::make_shared<ResizedImageDescriptor>(
            cv::Size(16, 32), cv::InterpolationFlags::INTER_LINEAR);
    std::shared_ptr<IDescriptorDistance> distance_fast =
        std::make_shared<MatchTemplateDistance>();

    tracker->set_descriptor_fast(descriptor_fast);
    tracker->set_distance_fast(distance_fast);

    if (!reid_model.empty() && !reid_weights.empty()) {
        Person_CnnConfig reid_config(reid_model, reid_weights);
        reid_config.max_batch_size = 16;

        std::shared_ptr<IImageDescriptor> descriptor_strong =
            std::make_shared<DescriptorIE>(reid_config, ie, deviceName);

        if (descriptor_strong == nullptr) {
            THROW_IE_EXCEPTION << "[SAMPLES] internal error - invalid descriptor";
        }
        std::shared_ptr<IDescriptorDistance> distance_strong =
            std::make_shared<CosDistance>(descriptor_strong->size());

        tracker->set_descriptor_strong(descriptor_strong);
        tracker->set_distance_strong(distance_strong);
    }
    else {
        std::cout << "WARNING: Either reid model or reid weights "
            << "were not specified. "
            << "Only fast reidentification approach will be used." << std::endl;
    }

    return tracker;
}
#endif

#if CU_MULTI_CAM
const size_t DISP_WIDTH = 1920;
const size_t DISP_HEIGHT = 1080;
const size_t MAX_INPUTS = 25;

struct DisplayParams {
    std::string name;
    cv::Size windowSize;
    cv::Size frameSize;
    size_t count;
    cv::Point points[MAX_INPUTS];
};

DisplayParams prepareDisplayParams(size_t count) {
    DisplayParams params;
    params.count = count;
    params.windowSize = cv::Size(DISP_WIDTH, DISP_HEIGHT);

    size_t gridCount = static_cast<size_t>(ceil(sqrt(count)));
    size_t gridStepX = static_cast<size_t>(DISP_WIDTH / gridCount);
    size_t gridStepY = static_cast<size_t>(DISP_HEIGHT / gridCount);
    params.frameSize = cv::Size(gridStepX, gridStepY);

    for (size_t i = 0; i < count; i++) {
        cv::Point p;
        p.x = gridStepX * (i / gridCount);
        p.y = gridStepY * (i%gridCount);
        params.points[i] = p;
    }
    return params;
}

#if CU_NIPA
static size_t fpsCounter = 0;
#endif

struct Face {
    cv::Rect2f rect;
    float confidence;
    unsigned char age;
    unsigned char gender;
#if CU_MULTI_CAM_PD //skc
    float score = 0.f;
#endif
    Face(cv::Rect2f r, float c, unsigned char a, unsigned char g) : rect(r), confidence(c), age(a), gender(g) {}
};

#if CU_MULTI_CAM_PD
void drawDetections(cv::Mat& img, const std::vector<Person_TrackedObject> detections) {
    for (const Person_TrackedObject& f : detections) {
        cv::Rect ri(static_cast<int>(f.rect.x), static_cast<int>(f.rect.y),
            static_cast<int>(f.rect.width), static_cast<int>(f.rect.height));
        cv::rectangle(img, ri, cv::Scalar(255, 0, 0), 2);
    }
}
#else
void drawDetections(cv::Mat& img, const std::vector<Face> detections) {
    for (const Face& f : detections) {
        cv::Rect ri(static_cast<int>(f.rect.x*img.cols), static_cast<int>(f.rect.y*img.rows),
            static_cast<int>(f.rect.width*img.cols), static_cast<int>(f.rect.height*img.rows));
        cv::rectangle(img, ri, cv::Scalar(255, 0, 0), 2);
    }
}
#endif

void displayNSources(const std::vector<std::shared_ptr<VideoFrame>>& data,
    float time,
    const std::string& stats,
    DisplayParams params) {
    cv::Mat windowImage = cv::Mat::zeros(params.windowSize, CV_8UC3);
    auto loopBody = [&](size_t i) {
        auto& elem = data[i];
        if (!elem->frame.empty()) {
            cv::Rect rectFrame = cv::Rect(params.points[i], params.frameSize);
            cv::Mat windowPart = windowImage(rectFrame);
            cv::resize(elem->frame, windowPart, params.frameSize);
#if !CU_MULTI_CAM_FRAME_ONLY
#if CU_MULTI_CAM_PD
            //drawDetections(windowPart, elem->detections.get<std::vector<Person_TrackedObject>>());
#else
            drawDetections(windowPart, elem->detections.get<std::vector<Face>>());
#endif
#endif
#if CU_MULTI_CAM_FACE //@@skc 4채널 person detect 만 하는데 2fps 정도밖에 안나오고 있다. 해상도를 줄여서 테스트 필요!!!
            for (auto r : elem->faces) {
                r.x = r.x / 2; // + params.points[i].x;
                r.y = r.y / 2; // + params.points[i].y;
                r.width /= 2; r.height /= 2;
                cv::rectangle(windowPart, r, cv::Scalar(0, 255, 0), 2);
            }
#endif
        }
    };
#if CU_NIPA
    fpsCounter++;
#endif

    auto drawStats = [&]() {
        if (FLAGS_show_stats && !stats.empty()) {
            static const cv::Point posPoint = cv::Point(3 * DISP_WIDTH / 4, 4 * DISP_HEIGHT / 5);
            auto pos = posPoint + cv::Point(0, 25);
            size_t currPos = 0;
            while (true) {
                auto newPos = stats.find('\n', currPos);
                cv::putText(windowImage, stats.substr(currPos, newPos - currPos), pos, cv::HersheyFonts::FONT_HERSHEY_COMPLEX, 0.8, cv::Scalar(0, 0, 255), 1);
                if (newPos == std::string::npos) {
                    break;
                }
                pos += cv::Point(0, 25);
                currPos = newPos + 1;
            }
        }
    };

    //  #ifdef USE_TBB
#if 0  // disable multithreaded rendering for now
    run_in_arena([&]() {
        tbb::parallel_for<size_t>(0, data.size(), [&](size_t i) {
            loopBody(i);
            });
        });
#else
    for (size_t i = 0; i < data.size(); ++i) {
        loopBody(i);
    }
#endif
    drawStats();

    char str[256];
    snprintf(str, sizeof(str), "%5.2f fps", static_cast<double>(1000.0f / time));
    cv::putText(windowImage, str, cv::Point(800, 100), cv::HersheyFonts::FONT_HERSHEY_COMPLEX, 2.0, cv::Scalar(0, 255, 0), 2);
    cv::imshow(params.name, windowImage);
}
#endif //CU_MULTI_CAM

#endif

using namespace InferenceEngine;

namespace {

    class Visualizer {
    private:
        cv::Mat frame_;
        cv::Mat top_persons_;
        const bool enabled_;
        const int num_top_persons_;
        cv::VideoWriter& writer_;
        float rect_scale_x_;
        float rect_scale_y_;
        static int const max_input_width_ = 1920;
        std::string const main_window_name_ = "Smart classroom demo";
        std::string const top_window_name_ = "Top-k students";
        static int const crop_width_ = 128;
        static int const crop_height_ = 320;
        static int const header_size_ = 80;
        static int const margin_size_ = 5;

    public:
        Visualizer(bool enabled, cv::VideoWriter& writer, int num_top_persons) : enabled_(enabled), num_top_persons_(num_top_persons), writer_(writer),
            rect_scale_x_(0), rect_scale_y_(0) {
            if (!enabled_) {
                return;
            }

            cv::namedWindow(main_window_name_);

            if (num_top_persons_ > 0) {
                cv::namedWindow(top_window_name_);

                CreateTopWindow();
                ClearTopWindow();
            }
        }

        static cv::Size GetOutputSize(const cv::Size& input_size) {
            if (input_size.width > max_input_width_) {
                float ratio = static_cast<float>(input_size.height) / input_size.width;
                return cv::Size(max_input_width_, cvRound(ratio*max_input_width_));
            }
            return input_size;
        }

        void SetFrame(const cv::Mat& frame) {
            if (!enabled_ && !writer_.isOpened()) {
                return;
            }

            frame_ = frame.clone();
            rect_scale_x_ = 1;
            rect_scale_y_ = 1;
            cv::Size new_size = GetOutputSize(frame_.size());
            if (new_size != frame_.size()) {
                rect_scale_x_ = static_cast<float>(new_size.height) / frame_.size().height;
                rect_scale_y_ = static_cast<float>(new_size.width) / frame_.size().width;
                cv::resize(frame_, frame_, new_size);
            }
        }

        void Show() const {
            if (enabled_) {
                cv::imshow(main_window_name_, frame_);
            }

            if (writer_.isOpened()) {
                writer_ << frame_;
            }
        }

        void DrawCrop(cv::Rect roi, int id, const cv::Scalar& color) const {
            if (!enabled_ || num_top_persons_ <= 0) {
                return;
            }

            if (id < 0 || id >= num_top_persons_) {
                return;
            }

            if (rect_scale_x_ != 1 || rect_scale_y_ != 1) {
                roi.x = cvRound(roi.x * rect_scale_x_);
                roi.y = cvRound(roi.y * rect_scale_y_);

                roi.height = cvRound(roi.height * rect_scale_y_);
                roi.width = cvRound(roi.width * rect_scale_x_);
            }

            roi.x = std::max(0, roi.x);
            roi.y = std::max(0, roi.y);
            roi.width = std::min(roi.width, frame_.cols - roi.x);
            roi.height = std::min(roi.height, frame_.rows - roi.y);

            const auto crop_label = std::to_string(id + 1);

            auto frame_crop = frame_(roi).clone();
            cv::resize(frame_crop, frame_crop, cv::Size(crop_width_, crop_height_));

            const int shift = (id + 1) * margin_size_ + id * crop_width_;
            frame_crop.copyTo(top_persons_(cv::Rect(shift, header_size_, crop_width_, crop_height_)));

            cv::imshow(top_window_name_, top_persons_);
        }

        void DrawObject(cv::Rect rect, const std::string& label_to_draw,
            const cv::Scalar& text_color, const cv::Scalar& bbox_color, bool plot_bg) {
            if (!enabled_ && !writer_.isOpened()) {
                return;
            }

            if (rect_scale_x_ != 1 || rect_scale_y_ != 1) {
                rect.x = cvRound(rect.x * rect_scale_x_);
                rect.y = cvRound(rect.y * rect_scale_y_);

                rect.height = cvRound(rect.height * rect_scale_y_);
                rect.width = cvRound(rect.width * rect_scale_x_);
            }
            cv::rectangle(frame_, rect, bbox_color);

            if (plot_bg && !label_to_draw.empty()) {
                int baseLine = 0;
                const cv::Size label_size =
                    cv::getTextSize(label_to_draw, cv::FONT_HERSHEY_PLAIN, 1, 1, &baseLine);
                cv::rectangle(frame_, cv::Point(rect.x, rect.y - label_size.height),
                    cv::Point(rect.x + label_size.width, rect.y + baseLine),
                    bbox_color, cv::FILLED);
            }
            if (!label_to_draw.empty()) {
                cv::putText(frame_, label_to_draw, cv::Point(rect.x, rect.y), cv::FONT_HERSHEY_PLAIN, 1,
                    text_color, 1, cv::LINE_AA);
            }
        }

        void DrawFPS(const float fps, const cv::Scalar& color) {
#if CU_NIPA // [skc] 파일로 저장시에는 화면에 FPS 가 표시되지 않고 있어서 표시되게 수정함
            if (enabled_) {
#else
            if (enabled_ && !writer_.isOpened()) {
#endif
                cv::putText(frame_,
                    std::to_string(static_cast<int>(fps)) + " fps",
                    cv::Point(10, 50), cv::FONT_HERSHEY_SIMPLEX, 2,
                    color, 2, cv::LINE_AA);
            }
        }

        void CreateTopWindow() {
            if (!enabled_ || num_top_persons_ <= 0) {
                return;
            }

            const int width = margin_size_ * (num_top_persons_ + 1) + crop_width_ * num_top_persons_;
            const int height = header_size_ + crop_height_ + margin_size_;

            top_persons_.create(height, width, CV_8UC3);
        }

        void ClearTopWindow() {
            if (!enabled_ || num_top_persons_ <= 0) {
                return;
            }

            top_persons_.setTo(cv::Scalar(255, 255, 255));

            for (int i = 0; i < num_top_persons_; ++i) {
                const int shift = (i + 1) * margin_size_ + i * crop_width_;

                cv::rectangle(top_persons_, cv::Point(shift, header_size_),
                    cv::Point(shift + crop_width_, header_size_ + crop_height_),
                    cv::Scalar(128, 128, 128), cv::FILLED);

                const auto label_to_draw = "#" + std::to_string(i + 1);
                int baseLine = 0;
                const auto label_size =
                    cv::getTextSize(label_to_draw, cv::FONT_HERSHEY_SIMPLEX, 2, 2, &baseLine);
                const int text_shift = (crop_width_ - label_size.width) / 2;
                cv::putText(top_persons_, label_to_draw,
                    cv::Point(shift + text_shift, label_size.height + baseLine / 2),
                    cv::FONT_HERSHEY_SIMPLEX, 2, cv::Scalar(0, 255, 0), 2, cv::LINE_AA);
            }

            cv::imshow(top_window_name_, top_persons_);
        }

        void Finalize() const {
            if (enabled_) {
                cv::destroyWindow(main_window_name_);

                if (num_top_persons_ > 0) {
                    cv::destroyWindow(top_window_name_);
                }
            }

            if (writer_.isOpened()) {
                writer_.release();
            }
        }
    };

#ifndef CU_DISABLE_ACTION_DETECTOR
    const int default_action_index = -1;  // Unknown action class

    void ConvertActionMapsToFrameEventTracks(const std::vector<std::map<int, int>>& obj_id_to_action_maps,
        int default_action,
        std::map<int, FrameEventsTrack>* obj_id_to_actions_track) {
        for (size_t frame_id = 0; frame_id < obj_id_to_action_maps.size(); ++frame_id) {
            for (const auto& tup : obj_id_to_action_maps[frame_id]) {
                if (tup.second != default_action) {
                    (*obj_id_to_actions_track)[tup.first].emplace_back(frame_id, tup.second);
                }
            }
        }
    }

    void SmoothTracks(const std::map<int, FrameEventsTrack>& obj_id_to_actions_track,
        int start_frame, int end_frame, int window_size, int min_length, int default_action,
        std::map<int, RangeEventsTrack>* obj_id_to_events) {
        // Iterate over face tracks
        for (const auto& tup : obj_id_to_actions_track) {
            const auto& frame_events = tup.second;
            if (frame_events.empty()) {
                continue;
            }

            RangeEventsTrack range_events;


            // Merge neighbouring events and filter short ones
            range_events.emplace_back(frame_events.front().frame_id,
                frame_events.front().frame_id + 1,
                frame_events.front().action);

            for (size_t frame_id = 1; frame_id < frame_events.size(); ++frame_id) {
                const auto& last_range_event = range_events.back();
                const auto& cur_frame_event = frame_events[frame_id];

                if (last_range_event.end_frame_id + window_size - 1 >= cur_frame_event.frame_id &&
                    last_range_event.action == cur_frame_event.action) {
                    range_events.back().end_frame_id = cur_frame_event.frame_id + 1;
                }
                else {
                    if (range_events.back().end_frame_id - range_events.back().begin_frame_id < min_length) {
                        range_events.pop_back();
                    }

                    range_events.emplace_back(cur_frame_event.frame_id,
                        cur_frame_event.frame_id + 1,
                        cur_frame_event.action);
                }
            }
            if (range_events.back().end_frame_id - range_events.back().begin_frame_id < min_length) {
                range_events.pop_back();
            }

            // Extrapolate track
            if (range_events.empty()) {
                range_events.emplace_back(start_frame, end_frame, default_action);
            }
            else {
                range_events.front().begin_frame_id = start_frame;
                range_events.back().end_frame_id = end_frame;
            }

            // Interpolate track
            for (size_t event_id = 1; event_id < range_events.size(); ++event_id) {
                auto& last_event = range_events[event_id - 1];
                auto& cur_event = range_events[event_id];

                int middle_point = static_cast<int>(0.5f * (cur_event.begin_frame_id + last_event.end_frame_id));

                cur_event.begin_frame_id = middle_point;
                last_event.end_frame_id = middle_point;
            }

            // Merge consecutive events
            auto& final_events = (*obj_id_to_events)[tup.first];
            final_events.push_back(range_events.front());
            for (size_t event_id = 1; event_id < range_events.size(); ++event_id) {
                const auto& cur_event = range_events[event_id];

                if (final_events.back().action == cur_event.action) {
                    final_events.back().end_frame_id = cur_event.end_frame_id;
                }
                else {
                    final_events.push_back(cur_event);
                }
            }
        }
    }

    void ConvertRangeEventsTracksToActionMaps(int num_frames,
        const std::map<int, RangeEventsTrack>& obj_id_to_events,
        std::vector<std::map<int, int>>* obj_id_to_action_maps) {
        obj_id_to_action_maps->resize(num_frames);

        for (const auto& tup : obj_id_to_events) {
            const int obj_id = tup.first;
            const auto& events = tup.second;

            for (const auto& event : events) {
                for (int frame_id = event.begin_frame_id; frame_id < event.end_frame_id; ++frame_id) {
                    (*obj_id_to_action_maps)[frame_id].emplace(obj_id, event.action);
                }
            }
        }
    }

    std::vector<std::string> ParseActionLabels(const std::string& in_str) {
        std::vector<std::string> labels;
        std::string label;
        std::istringstream stream_to_split(in_str);

        while (std::getline(stream_to_split, label, ',')) {
            labels.push_back(label);
        }

        return labels;
    }

    std::string GetActionTextLabel(const unsigned label, const std::vector<std::string>& actions_map) {
        if (label < actions_map.size()) {
            return actions_map[label];
        }
        return "__undefined__";
    }

    cv::Scalar GetActionTextColor(const unsigned label) {
        static std::vector<cv::Scalar> actions_map = {
            cv::Scalar(0, 255, 0), cv::Scalar(255, 0, 0), cv::Scalar(0, 0, 255), cv::Scalar(0, 255, 255) };
        if (label < actions_map.size()) {
            return actions_map[label];
        }
        return cv::Scalar(0, 0, 0);
    }
#endif

    float CalculateIoM(const cv::Rect& rect1, const cv::Rect& rect2) {
        int area1 = rect1.area();
        int area2 = rect2.area();

        float area_min = static_cast<float>(std::min(area1, area2));
        float area_intersect = static_cast<float>((rect1 & rect2).area());

        return area_intersect / area_min;
    }

    cv::Rect DecreaseRectByRelBorders(const cv::Rect& r) {
        float w = static_cast<float>(r.width);
        float h = static_cast<float>(r.height);

        float left = std::ceil(w * 0.0f);
        float top = std::ceil(h * 0.0f);
        float right = std::ceil(w * 0.0f);
        float bottom = std::ceil(h * .7f);

        cv::Rect res;
        res.x = r.x + static_cast<int>(left);
        res.y = r.y + static_cast<int>(top);
        res.width = static_cast<int>(r.width - left - right);
        res.height = static_cast<int>(r.height - top - bottom);
        return res;
    }

#ifndef CU_DISABLE_ACTION_DETECTOR
    int GetIndexOfTheNearestPerson(const TrackedObject& face, const std::vector<TrackedObject>& tracked_persons) {
        int argmax = -1;
        float max_iom = std::numeric_limits<float>::lowest();
        for (size_t i = 0; i < tracked_persons.size(); i++) {
            float iom = CalculateIoM(face.rect, DecreaseRectByRelBorders(tracked_persons[i].rect));
            if ((iom > 0) && (iom > max_iom)) {
                max_iom = iom;
                argmax = i;
            }
        }
        return argmax;
    }
#endif

#ifdef CU_ENABLE_PERSON_DETECTOR
    int Person_GetIndexOfTheNearestPerson(const TrackedObject& face, const Person_TrackedObjects& tracked_persons) {
        int argmax = -1;
        float max_iom = std::numeric_limits<float>::lowest();
        for (size_t i = 0; i < tracked_persons.size(); i++) {
            float iom = CalculateIoM(face.rect, DecreaseRectByRelBorders(tracked_persons[i].rect));
            if ((iom > 0) && (iom > max_iom)) {
                max_iom = iom;
                argmax = i;
            }
        }
        return argmax;
    }
#endif

    std::map<int, int> GetMapFaceTrackIdToLabel(const std::vector<Track>& face_tracks) {
        std::map<int, int> face_track_id_to_label;
        for (const auto& track : face_tracks) {
            const auto& first_obj = track.first_object;
            // check consistency
            // to receive this consistency for labels
            // use the function UpdateTrackLabelsToBestAndFilterOutUnknowns
            for (const auto& obj : track.objects) {
                SCR_CHECK_EQ(obj.label, first_obj.label);
                SCR_CHECK_EQ(obj.object_id, first_obj.object_id);
            }

            auto cur_obj_id = first_obj.object_id;
            auto cur_label = first_obj.label;
            SCR_CHECK(face_track_id_to_label.count(cur_obj_id) == 0) << " Repeating face tracks";
            face_track_id_to_label[cur_obj_id] = cur_label;
        }
        return face_track_id_to_label;
    }

}  // namespace

bool ParseAndCheckCommandLine(int argc, char *argv[]) {
    // ---------------------------Parsing and validation of input args--------------------------------------
#if CU_NIPA
    cu_set_param();
#endif

    gflags::ParseCommandLineNonHelpFlags(&argc, &argv, true);
    if (FLAGS_h) {
        showUsage();
        showAvailableDevices();
        return false;
    }

    slog::info << "Parsing input parameters" << slog::endl;

    if (FLAGS_i.empty()) {
        throw std::logic_error("Parameter -i is not set");
    }
    if (FLAGS_m_act.empty() && FLAGS_m_fd.empty()) {
        throw std::logic_error("At least one parameter -m_act or -m_fd must be set");
    }

    return true;
}

#if CU_MULTI_CAM_PD //skc
static cv::Rect TruncateToValidRect(const cv::Rect& rect,
    const cv::Size& size) {
    auto tl = rect.tl(), br = rect.br();
    tl.x = std::max(0, std::min(size.width - 1, tl.x));
    tl.y = std::max(0, std::min(size.height - 1, tl.y));
    br.x = std::max(0, std::min(size.width, br.x));
    br.y = std::max(0, std::min(size.height, br.y));
    int w = std::max(0, br.x - tl.x);
    int h = std::max(0, br.y - tl.y);
    return cv::Rect(tl.x, tl.y, w, h);
}

static cv::Rect IncreaseRect(const cv::Rect& r, float coeff_x,
    float coeff_y) {
    cv::Point2f tl = r.tl();
    cv::Point2f br = r.br();
    cv::Point2f c = (tl * 0.5f) + (br * 0.5f);
    cv::Point2f diff = c - tl;
    cv::Point2f new_diff{ diff.x * coeff_x, diff.y * coeff_y };
    cv::Point2f new_tl = c - new_diff;
    cv::Point2f new_br = c + new_diff;

    cv::Point new_tl_int{ static_cast<int>(std::floor(new_tl.x)), static_cast<int>(std::floor(new_tl.y)) };
    cv::Point new_br_int{ static_cast<int>(std::ceil(new_br.x)), static_cast<int>(std::ceil(new_br.y)) };

    return cv::Rect(new_tl_int, new_br_int);
}

static std::string skc_float_2_str(float f, int N/*자리수*/)
{
    std::ostringstream ss;
    ss << std::setprecision(N);
    ss << f;
    std::string s(ss.str());
    return s;
}

static void skc_DrawObject(cv::Mat& frame_, cv::Rect rect, const std::string& label_to_draw,
    const cv::Scalar& text_color, const cv::Scalar& bbox_color, bool plot_bg) {
    //if (!enabled_ && !writer_.isOpened()) {
    //    return;
    //}

    //if (rect_scale_x_ != 1 || rect_scale_y_ != 1) {
    //    rect.x = cvRound(rect.x * rect_scale_x_);
    //    rect.y = cvRound(rect.y * rect_scale_y_);

    //    rect.height = cvRound(rect.height * rect_scale_y_);
    //    rect.width = cvRound(rect.width * rect_scale_x_);
    //}
    cv::rectangle(frame_, rect, bbox_color);

    double fontScale = 2.0;
    if (plot_bg && !label_to_draw.empty()) {
        int baseLine = 0;
        const cv::Size label_size =
            cv::getTextSize(label_to_draw, cv::FONT_HERSHEY_PLAIN, fontScale, 1, &baseLine);
        cv::rectangle(frame_, cv::Point(rect.x, rect.y - label_size.height - baseLine), // 레이블 배경이 너무 아래 그려져서 -baseLine 시킴
            cv::Point(rect.x + label_size.width, rect.y + baseLine),
            bbox_color, cv::FILLED);
    }
    if (!label_to_draw.empty()) {
        cv::putText(frame_, label_to_draw, cv::Point(rect.x, rect.y), cv::FONT_HERSHEY_PLAIN, fontScale,
            text_color, 1, cv::LINE_AA);
    }
}
#endif

static std::vector<std::shared_ptr<PedestrianTracker>> s_pedestrian_tracker;

static void initPedestrianTracker(const std::string& person_reid_model, const std::string& person_reid_weights, Core& ie
    , const std::string& person_reid_mode, bool should_keep_tracking_info) {
    for (int i = 0; i < FLAGS_nc; i++) {
        s_pedestrian_tracker.push_back(
            CreatePedestrianTracker(person_reid_model, person_reid_weights, ie, person_reid_mode, should_keep_tracking_info));
    }
}

static std::shared_ptr<PedestrianTracker> getPedestrianTracker(int idx) {
    return s_pedestrian_tracker[idx];
}

int main(int argc, char* argv[]) {
    try {
        /** This demo covers 4 certain topologies and cannot be generalized **/
        slog::info << "InferenceEngine: " << GetInferenceEngineVersion() << slog::endl;

        if (!ParseAndCheckCommandLine(argc, argv)) {
            return 0;
        }

        const auto video_path = FLAGS_i;
        const auto ad_model_path = FLAGS_m_act;
        const auto ad_weights_path = fileNameNoExt(FLAGS_m_act) + ".bin";
        const auto fd_model_path = FLAGS_m_fd;
        const auto fd_weights_path = fileNameNoExt(FLAGS_m_fd) + ".bin";
        const auto fr_model_path = FLAGS_m_reid;
        const auto fr_weights_path = fileNameNoExt(FLAGS_m_reid) + ".bin";
        const auto lm_model_path = FLAGS_m_lm;
        const auto lm_weights_path = fileNameNoExt(FLAGS_m_lm) + ".bin";
        const auto teacher_id = FLAGS_teacher_id;

#if !CU_NIPA
        if (!FLAGS_teacher_id.empty() && !FLAGS_top_id.empty()) {
            slog::err << "Cannot run simultaneously teacher action and top-k students recognition."
                << slog::endl;
            return 1;
        }
#endif

        const auto num_top_persons = -1;

        slog::info << "Reading video '" << video_path << "'" << slog::endl;
        ImageGrabber cap(video_path);
#if !CU_MULTI_CAM
        if (!cap.IsOpened()) {
            slog::err << "Cannot open the video" << slog::endl;
            return 1;
        }
#endif

        slog::info << "Loading Inference Engine" << slog::endl;
        Core ie;

        std::vector<std::string> devices = { FLAGS_d_act, FLAGS_d_fd, FLAGS_d_lm,
                                            FLAGS_d_reid };
        std::set<std::string> loadedDevices;

        slog::info << "Device info: " << slog::endl;

        for (const auto &device : devices) {
            if (loadedDevices.find(device) != loadedDevices.end())
                continue;

            std::cout << ie.GetVersions(device) << std::endl;

            /** Load extensions for the CPU device **/
            if ((device.find("CPU") != std::string::npos)) {
                ie.AddExtension(std::make_shared<Extensions::Cpu::CpuExtensions>(), "CPU");

                if (!FLAGS_l.empty()) {
                    // CPU(MKLDNN) extensions are loaded as a shared library and passed as a pointer to base extension
                    auto extension_ptr = make_so_pointer<IExtension>(FLAGS_l);
                    ie.AddExtension(extension_ptr, "CPU");
                    slog::info << "CPU Extension loaded: " << FLAGS_l << slog::endl;
                }
            }
            else if (!FLAGS_c.empty()) {
                // Load Extensions for other plugins not CPU
                ie.SetConfig({ {PluginConfigParams::KEY_CONFIG_FILE, FLAGS_c} }, "GPU");
            }

            if (device.find("CPU") != std::string::npos) {
                ie.SetConfig({ {PluginConfigParams::KEY_DYN_BATCH_ENABLED, PluginConfigParams::YES} }, "CPU");
            }
            else if (device.find("GPU") != std::string::npos) {
                ie.SetConfig({ {PluginConfigParams::KEY_DYN_BATCH_ENABLED, PluginConfigParams::YES} }, "GPU");
            }

            if (FLAGS_pc)
                ie.SetConfig({ {PluginConfigParams::KEY_PERF_COUNT, PluginConfigParams::YES} });

            loadedDevices.insert(device);
        }

#ifndef CU_DISABLE_ACTION_DETECTOR
        // Load action detector
        ActionDetectorConfig action_config(ad_model_path, ad_weights_path);
        action_config.deviceName = FLAGS_d_act;
        action_config.ie = ie;
        action_config.is_async = true;
        action_config.enabled = !ad_model_path.empty();
        action_config.detection_confidence_threshold = static_cast<float>(FLAGS_t_ad);
        action_config.action_confidence_threshold = static_cast<float>(FLAGS_t_ar);
        action_config.num_action_classes = actions_map.size();
        ActionDetection action_detector(action_config);
#endif

        // Load face detector
        detection::DetectorConfig face_config(fd_model_path, fd_weights_path);
        face_config.deviceName = FLAGS_d_fd;
        face_config.ie = ie;
        face_config.is_async = true;
        face_config.enabled = !fd_model_path.empty();
        face_config.confidence_threshold = static_cast<float>(FLAGS_t_fd);
        face_config.input_h = FLAGS_inh_fd;
        face_config.input_w = FLAGS_inw_fd;
        face_config.increase_scale_x = static_cast<float>(FLAGS_exp_r_fd);
        face_config.increase_scale_y = static_cast<float>(FLAGS_exp_r_fd);
        detection::FaceDetection face_detector(face_config);

        // Load face detector for face database registration
        detection::DetectorConfig face_registration_det_config(fd_model_path, fd_weights_path);
        face_registration_det_config.deviceName = FLAGS_d_fd;
        face_registration_det_config.ie = ie;
        face_registration_det_config.enabled = !fd_model_path.empty();
        face_registration_det_config.is_async = false;
        face_registration_det_config.confidence_threshold = static_cast<float>(FLAGS_t_reg_fd);
        face_registration_det_config.increase_scale_x = static_cast<float>(FLAGS_exp_r_fd);
        face_registration_det_config.increase_scale_y = static_cast<float>(FLAGS_exp_r_fd);
        detection::FaceDetection face_detector_for_registration(face_registration_det_config);

        // Load face reid
        CnnConfig reid_config(fr_model_path, fr_weights_path);
        reid_config.max_batch_size = 16;
        reid_config.enabled = face_config.enabled && !fr_model_path.empty() && !lm_model_path.empty();
        reid_config.deviceName = FLAGS_d_reid;
        reid_config.ie = ie;
        VectorCNN face_reid(reid_config);

        // Load landmarks detector
        CnnConfig landmarks_config(lm_model_path, lm_weights_path);
        landmarks_config.max_batch_size = 16;
        landmarks_config.enabled = face_config.enabled && reid_config.enabled && !lm_model_path.empty();
        landmarks_config.deviceName = FLAGS_d_lm;
        landmarks_config.ie = ie;
        VectorCNN landmarks_detector(landmarks_config);

#ifdef CU_ENABLE_PERSON_DETECTOR
        auto det_model = FLAGS_m_person_det;
        auto det_weights = fileNameNoExt(FLAGS_m_person_det) + ".bin";
        auto detector_mode = "CPU"; //skc FLAGS_d_det;
        auto person_reid_mode = "CPU"; //skc FLAGS_d_reid;

        Person_DetectorConfig detector_confid(det_model, det_weights);
        Person_ObjectDetector pedestrian_detector(detector_confid, ie, detector_mode); //@@skc 불필요???

        auto person_reid_model = FLAGS_m_person_reid;
        auto person_reid_weights = fileNameNoExt(FLAGS_m_reid) + ".bin";

        bool should_keep_tracking_info = false; //skc should_save_det_log || should_print_out;
        //std::unique_ptr<PedestrianTracker> pedestrian_tracker =
        //    CreatePedestrianTracker(person_reid_model, person_reid_weights, ie, person_reid_mode, should_keep_tracking_info);
#if CU_MULTI_CAM_PD
        initPedestrianTracker(person_reid_model, person_reid_weights, ie, person_reid_mode, should_keep_tracking_info);
#endif
#endif

        // Create face gallery
        EmbeddingsGallery face_gallery(FLAGS_fg, FLAGS_t_reid, FLAGS_min_size_fr, FLAGS_crop_gallery,
            face_detector_for_registration, landmarks_detector, face_reid);

        if (!reid_config.enabled) {
            slog::warn << "Face recognition models are disabled!" << slog::endl;
        }
        else if (!face_gallery.size()) {
            slog::warn << "Face reid gallery is empty!" << slog::endl;
        }
        else {
            slog::info << "Face reid gallery size: " << face_gallery.size() << slog::endl;
        }

#if CU_MULTI_CAM
        try {
            std::string weightsPath;
            std::string modelPath = FLAGS_m;
            std::size_t found = modelPath.find_last_of(".");
            if (found > modelPath.size()) {
                slog::info << "Invalid model name: " << modelPath << slog::endl;
                slog::info << "Expected to be <model_name>.xml" << slog::endl;
                return -1;
            }
            weightsPath = modelPath.substr(0, found) + ".bin";
            slog::info << "Model   path: " << modelPath << slog::endl;
            slog::info << "Weights path: " << weightsPath << slog::endl;

            IEGraph::InitParams graphParams;
            graphParams.batchSize = FLAGS_bs;
            graphParams.maxRequests = FLAGS_n_ir;
            graphParams.collectStats = FLAGS_show_stats;
            graphParams.reportPerf = FLAGS_pc;
            graphParams.modelPath = modelPath;
            graphParams.weightsPath = weightsPath;
            graphParams.cpuExtPath = FLAGS_l;
            graphParams.cldnnConfigPath = FLAGS_c;
            graphParams.deviceName = FLAGS_d;

            std::shared_ptr<IEGraph> network(new IEGraph(graphParams));
            auto inputDims = network->getInputDims();
            if (4 != inputDims.size()) {
                throw std::runtime_error("Invalid network input dimensions");
            }
#if CU_MULTI_CAM_FACE
            network->face_detector = &face_detector;
#endif

            std::vector<std::string> files;
            parseInputFilesArguments(files);

            slog::info << "\tNumber of input web cams:    " << FLAGS_nc << slog::endl;
            slog::info << "\tNumber of input video files: " << files.size() << slog::endl;
            slog::info << "\tDuplication multiplayer:     " << FLAGS_duplicate_num << slog::endl;

            const auto duplicateFactor = (1 + FLAGS_duplicate_num);
            size_t numberOfInputs = (FLAGS_nc + files.size()) * duplicateFactor;

            DisplayParams params = prepareDisplayParams(numberOfInputs);

            slog::info << "\tNumber of input channels:    " << numberOfInputs << slog::endl;
            if (numberOfInputs > MAX_INPUTS) {
                throw std::logic_error("Number of inputs exceed maximum value [25]");
            }

            VideoSources::InitParams vsParams;
            vsParams.queueSize = FLAGS_n_iqs;
            vsParams.collectStats = FLAGS_show_stats;
            vsParams.realFps = FLAGS_real_input_fps;
            vsParams.expectedHeight = static_cast<unsigned>(inputDims[2]);
            vsParams.expectedWidth = static_cast<unsigned>(inputDims[3]);

            VideoSources sources(vsParams);
            if (!files.empty()) {
                slog::info << "Trying to open input video ..." << slog::endl;
                for (auto& file : files) {
                    try {
                        sources.openVideo(file, false);
                    }
                    catch (...) {
                        slog::info << "Cannot open video [" << file << "]" << slog::endl;
                        throw;
                    }
                }
            }

            cv::Size skc_movie_size(params.windowSize);

            if (FLAGS_nc) {
                slog::info << "Trying to connect " << FLAGS_nc << " web cams ..." << slog::endl;
                for (size_t i = 0; i < FLAGS_nc; ++i) {
                    try {
#if CU_NIPA
#if !true
                        //sources.openVideo(cu_convert_path("/../../model/classroom.mp4"), true);
                        std::string s = "/../../model/classroom_" + std::to_string(i) + ".mp4";
                        skc_movie_size = cv::Size(1920, 1080);
                        //std::string s = "/../../model/skc_1person_" + std::to_string(i) + ".mp4";
                        //skc_movie_size = cv::Size(640, 480);
                        //std::string s = "/../../model/4p-c" + std::to_string(i) + ".avi";
                        //skc_movie_size = cv::Size(360, 288);
#if !true
                        sources.openVideo(cu_convert_path(s), true);
#else
                        sources.openVideo("0", true); // "0" -> 로컬 카메라 0
                        skc_movie_size = cv::Size(640, 480);
#endif
#else
                        //sources.openVideo("rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov", true);
                        if (i == 0) sources.openVideo("rtsp://admin:cubox2019!1@172.16.160.231:554/Straming/Channels/101", true);
                        else if (i == 1) sources.openVideo("rtsp://admin:cubox2019!1@172.16.160.232:554/Straming/Channels/101", true);
                        else if (i == 2) sources.openVideo("rtsp://admin:cubox2019!1@172.16.160.233:554/Straming/Channels/101", true);
                        else if (i == 3) sources.openVideo("rtsp://admin:cubox2019!1@172.16.160.234:554/Straming/Channels/101", true);
#endif
#else
                        sources.openVideo(std::to_string(i), true);
#endif
                    }
                    catch (...) {
                        slog::info << "Cannot open web cam [" << i << "]" << slog::endl;
                        throw;
                    }
                }
            }
            sources.start();

            size_t currentFrame = 0;

            network->start([&](VideoFrame& img) {
                img.sourceIdx = currentFrame;
                auto camIdx = currentFrame / duplicateFactor;
                currentFrame = (currentFrame + 1) % numberOfInputs;
                return sources.getFrame(camIdx, img);
#if CU_MULTI_CAM_FRAME_ONLY
                }, nullptr);
#else
            }, [](InferenceEngine::InferRequest::Ptr req, const std::vector<std::string>& outputDataBlobNames, cv::Size frameSize, int skc_frameIdx) {
#if CU_MULTI_CAM_PD //skc
            auto output = req->GetBlob(outputDataBlobNames[0]);

            const float *data = req->GetBlob(outputDataBlobNames[0])->buffer().as<float *>();

            auto a = output->getTensorDesc();
            auto b = getTensorHeight(a);

            const SizeVector outputDims = output->getTensorDesc().getDims(); // 1, 1, 200, 7
            int max_detections_count_ = outputDims[2]; // 200
            int object_size_ = outputDims[3];

            InferenceEngine::SizeVector svec = output->getTensorDesc().getDims();
            size_t total = 1;
            for (auto v : svec) {
                total *= v; // 1400 = 1*1*200*7
            }

#define SSD_EMPTY_DETECTIONS_INDICATOR -1.0
            float width_ = static_cast<float>(frameSize.width);
            float height_ = static_cast<float>(frameSize.height);
            std::vector<Detections> detections(getTensorHeight(output->getTensorDesc()) / 200);
            //std::vector<Detections> detections(max_detections_count_); // getTensorHeight(output->getTensorDesc()) = 200
            for (auto& d : detections) {
                d.set(new std::vector<Person_TrackedObject>);
            }

            int skc_cnt = 0;
            for (int det_id = 0; det_id < max_detections_count_; ++det_id) {
                const int start_pos = det_id * object_size_;

                const float batchID = data[start_pos];
                if (batchID == SSD_EMPTY_DETECTIONS_INDICATOR) {
                    break;
                }

                const float score = std::min(std::max(0.0f, data[start_pos + 2]), 1.0f);
                const float x0 =
                    std::min(std::max(0.0f, data[start_pos + 3]), 1.0f) * width_;
                const float y0 =
                    std::min(std::max(0.0f, data[start_pos + 4]), 1.0f) * height_;
                const float x1 =
                    std::min(std::max(0.0f, data[start_pos + 5]), 1.0f) * width_;
                const float y1 =
                    std::min(std::max(0.0f, data[start_pos + 6]), 1.0f) * height_;

                int idxInBatch = static_cast<int>(data[start_pos]);

                Person_TrackedObject object;
                object.confidence = score;
                object.rect = cv::Rect(cv::Point(static_cast<int>(round(static_cast<double>(x0))),
                    static_cast<int>(round(static_cast<double>(y0)))),
                    cv::Point(static_cast<int>(round(static_cast<double>(x1))),
                        static_cast<int>(round(static_cast<double>(y1)))));

                object.rect = TruncateToValidRect(IncreaseRect(object.rect,
                    1.f/*config_.increase_scale_x*/,
                    1.f/*config_.increase_scale_y*/),
                    cv::Size(static_cast<int>(width_), static_cast<int>(height_)));
                object.frame_idx = skc_frameIdx;

                if (object.confidence > 0.5f/*config_.confidence_threshold*/ && object.rect.area() > 0) {
                    detections[idxInBatch].get<std::vector<Person_TrackedObject>>().emplace_back(object);
                    skc_cnt++;
                }
            }
            return detections;
#else
            auto output = req->GetBlob(outputDataBlobNames[0]);

            float* dataPtr = output->buffer();
            InferenceEngine::SizeVector svec = output->getTensorDesc().getDims();
            size_t total = 1;
            for (auto v : svec) {
                total *= v;
            }


            std::vector<Detections> detections(getTensorHeight(output->getTensorDesc()) / 200);
            for (auto& d : detections) {
                d.set(new std::vector<Face>);
            }

            for (size_t i = 0; i < total; i += 7) {
                float conf = dataPtr[i + 2];
                if (conf > FLAGS_t) {
                    int idxInBatch = static_cast<int>(dataPtr[i]);
                    float x0 = std::min(std::max(0.0f, dataPtr[i + 3]), 1.0f);
                    float y0 = std::min(std::max(0.0f, dataPtr[i + 4]), 1.0f);
                    float x1 = std::min(std::max(0.0f, dataPtr[i + 5]), 1.0f);
                    float y1 = std::min(std::max(0.0f, dataPtr[i + 6]), 1.0f);

                    cv::Rect2f rect = { x0 , y0, x1 - x0, y1 - y0 };
                    detections[idxInBatch].get<std::vector<Face>>().emplace_back(rect, conf, 0, 0);
        }
            }
            return detections;
#endif
        });
#endif
            network->setDetectionConfidence(static_cast<float>(FLAGS_t));

            std::atomic<float> averageFps = { 0.0f };

            std::vector<std::shared_ptr<VideoFrame>> batchRes;

            std::mutex statMutex;
            std::stringstream statStream;

            std::cout << "To close the application, press 'CTRL+C' here";
            if (!FLAGS_no_show) {
                std::cout << " or switch to the output window and press ESC key";
            }
            std::cout << std::endl;

            const size_t outputQueueSize = 1;
            AsyncOutput output(FLAGS_show_stats, outputQueueSize,
                [&](const std::vector<std::shared_ptr<VideoFrame>>& result) {
                    std::string str;
                    if (FLAGS_show_stats) {
                        std::unique_lock<std::mutex> lock(statMutex);
                        str = statStream.str();
                    }
#if CU_MULTI_CAM_PD
                    bool cu_every_person_has_face_id = false;
                    Person_TrackedObjects detections;
                    for (size_t chId = 0; chId < result.size(); ++chId) {
                        auto& vf = result[chId];
                        auto objs = vf->vf_detections.get<std::vector<Person_TrackedObject>>();
                        detections.clear();
                        for (auto& o : objs) {
                            auto r = o.rect;
                            //o.rect = cv::Rect(r.x * 2, r.y * 2, r.width * 2, r.height * 2); //skc test
                            detections.push_back(o);
                        }

                        double video_fps = 60.f; //skc test cap.GetFPS();
                        int skc_frameIdx = vf->skc_frameIdx;
                        size_t skc_camIdx = vf->sourceIdx;
                        cv::Mat& frame = vf->frame;
                        auto pt = getPedestrianTracker(skc_camIdx);

                        // timestamp in milliseconds
                        uint64_t cur_timestamp = static_cast<uint64_t>(1000.0 / video_fps * skc_frameIdx);
                        pt->Process(frame, detections, cur_timestamp);

                        // face id 지정이 안된 person box 가 있는지 여부 체크
                        int cnt, total;
                        if (pt->AllTrackHasFaceId(cnt, total)) {
                            cu_every_person_has_face_id = true;
                        }
                        std::string text = "matched " + std::to_string(cnt) + "/" + std::to_string(total);
                        cv::putText(frame, text, cv::Point(200, 50), cv::FONT_HERSHEY_SIMPLEX, 1.5, cv::Scalar(255, 255, 0), 2, cv::LINE_AA);

                        bool should_show = true;
                        if (should_show) {
                            // Drawing colored "worms" (tracks).
                            frame = pt->DrawActiveTracks(frame);

                            // Drawing all detected objects on a frame by BLUE COLOR
                            for (const auto &detection : detections) {
                                cv::rectangle(frame, detection.rect, cv::Scalar(255, 0, 0), 3);
                            }

                            // Drawing tracked detections only by RED color and print ID and detection
                            // confidence level.
                            for (const auto &detection : pt->TrackedDetections()) {
                                cv::rectangle(frame, detection.rect, cv::Scalar(0, 0, 255), 2);
                                //std::string text = std::to_string(detection.object_id) + " conf: " + std::to_string(detection.confidence);
                                std::string face_label = face_gallery.GetLabelByID(pt->GetTrackFaceId(detection.rect));
                                auto score = skc_float_2_str(detection.confidence * 100, 4);
                                std::string text = face_label +" (" + score + ")"; // conf: 
                                cv::Point pt = detection.rect.tl(); pt.y -= 10;
                                cv::putText(frame, text, pt, cv::FONT_HERSHEY_PLAIN, 2.0, cv::Scalar(0, 255, 255), 1, cv::LINE_AA);
                            }
                        }

#if true //CU_MULTI_CAM_FACE, CU_PERF: 여길 안태우면 1ch 25 fps, 태우면 5 fps
                        face_detector.enqueue(frame);
                        face_detector.submitRequest();
                        face_detector.wait();
                        face_detector.fetchResults();
                        detection::DetectedObjects faces = face_detector.results;
                        
                        std::vector<cv::Mat> face_rois, landmarks, embeddings;
                        //TrackedObjects tracked_face_objects; //CU_DISABLE_ACTION_DETECTOR

                        for (const auto& face : faces) {
                            //face_rois.push_back(prev_frame(face.rect));
                            face_rois.push_back(frame(face.rect).clone()); // [skc] 얼굴영역이 틀어져 보이므로 clone() 시킴
                        }
#if !false // CU_PERF: 여길 안태우면 1ch 8 fps
                        landmarks_detector.Compute(face_rois, &landmarks, cv::Size(2, 5));
                        AlignFaces(&face_rois, &landmarks);
                        face_reid.Compute(face_rois, &embeddings);
                        //auto ids = face_gallery.GetIDsByEmbeddings(embeddings);
                        auto ids = face_gallery.GetIDsByEmbeddingsEx(embeddings, !true);

#ifdef CU_ENABLE_PERSON_DETECTOR
                        // face box 를 그리고, person box 에 face id 를 지정
                        for (size_t i = 0; i < ids.size(); ++i) {
                            int id = ids[i].id;
                            if (id == EmbeddingsGallery::unknown_id) {
                                slog::info << "EmbeddingsGallery::unknown_id i=" << i << slog::endl;
                                break;
                            }
                            const auto& face = faces[i];
                            std::string label_to_draw = face_gallery.GetLabelByID(id);
                            // TODO: 점수 획득 필요(EmbeddingsGallery::m_distances 추가 필요)
                            //label_to_draw += " (" + std::to_string(face.confidence * 100) + ")";
                            auto score = skc_float_2_str((1 - ids[i].distance) * 100, 4);
                            label_to_draw += " (" + score + ")";
                            auto textColor = cv::Scalar(128, 128, 128);
                            if (ids[i].distance <= FLAGS_t_reid)
                                textColor = cv::Scalar(0, 0, 255);
                            skc_DrawObject(frame, face.rect, label_to_draw, textColor, cv::Scalar(255, 255, 255), true); // 얼굴 박스 그리기
                            //cv::rectangle(frame, face.rect, cv::Scalar(0, 255, 255), 2);
                            slog::info << "@@skc -------------------> label_to_draw=" << label_to_draw << slog::endl;

                            // person box 에 face id 를 지정
                            getPedestrianTracker(chId)->SetTrackFaceId(face.rect, id);
                        }
#endif
#endif //false
#endif
                    }
#endif

                    displayNSources(result, averageFps, str, params);
#if CU_NIPA
                    //static int _cnt = 0;
                    //slog::info << "@@skc displayNSources cnt=" + std::to_string(_cnt++) + "\n";
#endif

                    return (cv::waitKey(1) != 27);
                });

            output.start();

            using timer = std::chrono::high_resolution_clock;
            using duration = std::chrono::duration<float, std::milli>;
            timer::time_point lastTime = timer::now();
            duration samplingTimeout(FLAGS_fps_sp);

#if !CU_NIPA
            size_t fpsCounter = 0;
#endif

            size_t perfItersCounter = 0;

            while (true) {
                bool readData = true;
                while (readData) {
#if CU_NIPA
                    //auto br = network->getBatchData(params.windowSize); // [skc] 4ch 좌표 문제 수정
                    auto br = network->getBatchData(skc_movie_size); // [skc] 4ch 좌표 문제 수정, 동영상 크기로 설정해줘야한다.
#else
                    auto br = network->getBatchData(params.frameSize);
#endif
                    for (size_t i = 0; i < br.size(); i++) {
                        auto val = static_cast<unsigned int>(br[i]->sourceIdx);
                        auto it = find_if(batchRes.begin(), batchRes.end(), [val](const std::shared_ptr<VideoFrame>& vf) { return vf->sourceIdx == val; });
                        if (it != batchRes.end()) {
                            if (!FLAGS_no_show) {
                                //skc 채널개수(FLAGS_nc)만큼 채워지면 output.push() 를 호출함
                                output.push(std::move(batchRes));
                            }
                            batchRes.clear();
                            readData = false;
                        }
                        batchRes.push_back(std::move(br[i]));
                    }
                }
#if !CU_NIPA
                ++fpsCounter;
#endif

                if (!output.isAlive()) {
                    break;
                }

                auto currTime = timer::now();
                auto deltaTime = (currTime - lastTime);
                if (deltaTime >= samplingTimeout) {
                    auto durMsec =
                        std::chrono::duration_cast<duration>(deltaTime).count();
                    auto frameTime = durMsec / static_cast<float>(fpsCounter);
                    slog::info << "@@skc fpsCounter=" + std::to_string(fpsCounter) + ", durMsec=" + std::to_string(durMsec) + "ms, FPS=" + std::to_string(1000.f / frameTime) + "\n";
                    fpsCounter = 0;
                    lastTime = currTime;

                    if (FLAGS_no_show) {
                        slog::info << "Average Throughput : " << 1000.f / frameTime << " fps" << slog::endl;
                        if (++perfItersCounter >= FLAGS_n_sp) {
                            break;
                        }
                    }
                    else {
                        averageFps = frameTime;
                    }

                    if (FLAGS_show_stats) {
                        auto inputStat = sources.getStats();
                        auto inferStat = network->getStats();
                        auto outputStat = output.getStats();

                        std::unique_lock<std::mutex> lock(statMutex);
                        statStream.str(std::string());
                        statStream << std::fixed << std::setprecision(1);
                        statStream << "Input reads: ";
                        for (size_t i = 0; i < inputStat.readTimes.size(); ++i) {
                            if (0 == (i % 4)) {
                                statStream << std::endl;
                            }
                            statStream << inputStat.readTimes[i] << "ms ";
                        }
                        statStream << std::endl;
                        statStream << "HW decoding latency: "
                            << inputStat.decodingLatency << "ms";
                        statStream << std::endl;
                        statStream << "Preprocess time: "
                            << inferStat.preprocessTime << "ms";
                        statStream << std::endl;
                        statStream << "Plugin latency: "
                            << inferStat.inferTime << "ms";
                        statStream << std::endl;

                        statStream << "Render time: " << outputStat.renderTime
                            << "ms" << std::endl;

                        if (FLAGS_no_show) {
                            slog::info << statStream.str() << slog::endl;
                        }
                    }
                }
            }
            network.reset();
        }
        catch (const std::exception& error) {
            slog::err << error.what() << slog::endl;
            return 1;
        }
        catch (...) {
            slog::err << "Unknown/internal exception happened." << slog::endl;
            return 1;
        }

        slog::info << "Execution successful" << slog::endl;
        if (true)
            return 0;
#endif // CU_MULTI_CAM

#ifndef CU_DISABLE_ACTION_DETECTOR
        // Create tracker for reid
        TrackerParams tracker_reid_params;
        tracker_reid_params.min_track_duration = 1;
        tracker_reid_params.forget_delay = 150;
        tracker_reid_params.affinity_thr = 0.8f;
        tracker_reid_params.averaging_window_size_for_rects = 1;
        tracker_reid_params.averaging_window_size_for_labels = std::numeric_limits<int>::max();
        tracker_reid_params.bbox_heights_range = cv::Vec2f(10, 1080);
        tracker_reid_params.drop_forgotten_tracks = false;
        tracker_reid_params.max_num_objects_in_track = std::numeric_limits<int>::max();
        tracker_reid_params.objects_type = "face";

        Tracker tracker_reid(tracker_reid_params);

        // Create Tracker for action recognition
        TrackerParams tracker_action_params;
        tracker_action_params.min_track_duration = 8;
        tracker_action_params.forget_delay = 150;
        tracker_action_params.affinity_thr = 0.9f;
        tracker_action_params.averaging_window_size_for_rects = 5;
        tracker_action_params.averaging_window_size_for_labels = FLAGS_ss_t > 0
            ? FLAGS_ss_t
            : actions_type == TOP_K ? 5 : 1;
        tracker_action_params.bbox_heights_range = cv::Vec2f(10, 2160);
        tracker_action_params.drop_forgotten_tracks = false;
        tracker_action_params.max_num_objects_in_track = std::numeric_limits<int>::max();
        tracker_action_params.objects_type = "action";

        Tracker tracker_action(tracker_action_params);
#endif

        cv::Mat frame, prev_frame;
        DetectedActions actions;
        detection::DetectedObjects faces;

        float work_time_ms = 0.f;
        float wait_time_ms = 0.f;
        size_t work_num_frames = 0;
        size_t wait_num_frames = 0;
        size_t total_num_frames = 0;
        const char ESC_KEY = 27;
        const char SPACE_KEY = 32;
        const cv::Scalar green_color(0, 255, 0);
        const cv::Scalar red_color(0, 0, 255);
        const cv::Scalar white_color(255, 255, 255);
#ifndef CU_DISABLE_ACTION_DETECTOR
        std::vector<std::map<int, int>> face_obj_id_to_action_maps;

        int teacher_track_id = -1;
#endif

        if (cap.GrabNext()) {
            cap.Retrieve(frame);
        }
        else {
            slog::err << "Can't read the first frame" << slog::endl;
            return 1;
        }

#if CU_NIPA
        // [skc] 아래에서 최초에 face_detector.fetchResults() 호출시 성공을 위해 필요함
        face_detector.enqueue(frame);
        face_detector.submitRequest();
#endif

        prev_frame = frame.clone();

        bool is_last_frame = false;
        bool is_monitoring_enabled = false;
        auto prev_frame_path = cap.GetVideoPath();

        cv::VideoWriter vid_writer;
        if (!FLAGS_out_v.empty()) {
            vid_writer = cv::VideoWriter(FLAGS_out_v, cv::VideoWriter::fourcc('M', 'J', 'P', 'G'),
                cap.GetFPS(), Visualizer::GetOutputSize(frame.size()));
        }
        Visualizer sc_visualizer(!FLAGS_no_show, vid_writer, num_top_persons);
        DetectionsLogger logger(std::cout, FLAGS_r, FLAGS_ad, FLAGS_al);

#ifndef CU_DISABLE_ACTION_DETECTOR
        const int smooth_window_size = static_cast<int>(cap.GetFPS() * FLAGS_d_ad);
        const int smooth_min_length = static_cast<int>(cap.GetFPS() * FLAGS_min_ad);
#endif

        std::cout << "To close the application, press 'CTRL+C' here";
        if (!FLAGS_no_show) {
            std::cout << " or switch to the output window and press ESC key";
        }
        std::cout << std::endl;

        while (!is_last_frame) {
            logger.CreateNextFrameRecord(cap.GetVideoPath(), work_num_frames, prev_frame.cols, prev_frame.rows);
            auto started = std::chrono::high_resolution_clock::now();

            is_last_frame = !cap.GrabNext();
            if (!is_last_frame)
                cap.Retrieve(frame);

            char key = cv::waitKey(1);
            if (key == ESC_KEY) {
                break;
            }

            sc_visualizer.SetFrame(prev_frame);
#ifdef CU_TEST_DRAW_FRAME_ONLY
            if (true) {
                auto elapsed = std::chrono::high_resolution_clock::now() - started;
                auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
                work_time_ms += elapsed_ms;

                sc_visualizer.DrawFPS(1e3f / (work_time_ms / static_cast<float>(work_num_frames) + 1e-6f),
                    green_color);

                ++work_num_frames;
                ++total_num_frames;

                sc_visualizer.Show();
                prev_frame = frame.clone();
                continue;
            }
#endif

            {
                bool cu_every_person_has_face_id = false;
#ifdef CU_ENABLE_PERSON_DETECTOR
                const cv::Scalar blue_color(255, 0, 0);
                const cv::Scalar yellow_color(0, 255, 255);

                int frame_idx = work_num_frames;
                pedestrian_detector.submitFrame(frame, frame_idx);
                pedestrian_detector.waitAndFetchResults();
                Person_TrackedObjects detections = pedestrian_detector.getResults();
                for (size_t i = 0; i < detections.size(); ++i) {
                    const auto& d = detections[i];
                    //sc_visualizer.DrawObject(d.rect, std::to_string(d.confidence), blue_color, yellow_color, true); // person box 그리기
                }

                double video_fps = cap.GetFPS();
                // timestamp in milliseconds
                uint64_t cur_timestamp = static_cast<uint64_t>(1000.0 / video_fps * frame_idx);
                getPedestrianTracker(0)->Process(frame, detections, cur_timestamp);

                // face id 지정이 안된 person box 가 있는지 여부 체크
                int cnt, total;
                if (getPedestrianTracker(0)->AllTrackHasFaceId(cnt, total)) {
                    cu_every_person_has_face_id = true;
                }
                std::string text = "matched " + std::to_string(cnt) + "/" + std::to_string(total);
                cv::putText(frame, text, cv::Point(200, 50), cv::FONT_HERSHEY_SIMPLEX, 1.5, cv::Scalar(255, 255, 0), 2, cv::LINE_AA);

                bool should_show = true;
                if (should_show) {
                    // Drawing colored "worms" (tracks).
                    frame = getPedestrianTracker(0)->DrawActiveTracks(frame);

                    // Drawing all detected objects on a frame by BLUE COLOR
                    for (const auto &detection : detections) {
                        cv::rectangle(frame, detection.rect, cv::Scalar(255, 0, 0), 3);
                    }

                    // Drawing tracked detections only by RED color and print ID and detection
                    // confidence level.
                    for (const auto &detection : getPedestrianTracker(0)->TrackedDetections()) {
                        cv::rectangle(frame, detection.rect, cv::Scalar(0, 0, 255), 2);
                        //std::string text = std::to_string(detection.object_id) + " conf: " + std::to_string(detection.confidence);
                        std::string face_label = face_gallery.GetLabelByID(getPedestrianTracker(0)->GetTrackFaceId(detection.rect));
                        std::string text = face_label + " (" + std::to_string(detection.confidence) + ")"; // conf: 
                        cv::Point pt = detection.rect.tl(); pt.y -= 10;
                        cv::putText(frame, text, pt, cv::FONT_HERSHEY_PLAIN, 2.0, cv::Scalar(0, 255, 255), 1, cv::LINE_AA);
                    }
                }
#endif

#ifdef CU_DISABLE_FACE_DETECTOR
                cu_every_person_has_face_id = true; // person box 만 처리하도록 강제함
#endif
                if (cu_every_person_has_face_id) {
                    cv::putText(frame, "Tracking only!!!", cv::Point(10, 100), cv::FONT_HERSHEY_SIMPLEX, 2, cv::Scalar(0, 255, 0), 2, cv::LINE_AA);
                    // only show FPS
                    auto elapsed = std::chrono::high_resolution_clock::now() - started;
                    auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
                    work_time_ms += elapsed_ms;
                }
                else {
                    face_detector.wait();
                    face_detector.fetchResults();
                    faces = face_detector.results;

#ifndef CU_DISABLE_ACTION_DETECTOR
                    action_detector.wait();
                    action_detector.fetchResults();
                    actions = action_detector.results;
#endif

                    if (!is_last_frame) {
                        prev_frame_path = cap.GetVideoPath();
                        face_detector.enqueue(frame);
                        face_detector.submitRequest();
#ifndef CU_DISABLE_ACTION_DETECTOR
                        action_detector.enqueue(frame);
                        action_detector.submitRequest();
#endif
                    }

                    std::vector<cv::Mat> face_rois, landmarks, embeddings;
                    //TrackedObjects tracked_face_objects; //CU_DISABLE_ACTION_DETECTOR

                    for (const auto& face : faces) {
                        face_rois.push_back(prev_frame(face.rect));
                    }
                    landmarks_detector.Compute(face_rois, &landmarks, cv::Size(2, 5));
                    AlignFaces(&face_rois, &landmarks);
                    face_reid.Compute(face_rois, &embeddings);
                    auto ids = face_gallery.GetIDsByEmbeddings(embeddings);

#ifdef CU_ENABLE_PERSON_DETECTOR
                    // face box 를 그리고, person box 에 face id 를 지정
                    for (size_t i = 0; i < ids.size(); ++i) {
                        int id = ids[i];
                        if (id == EmbeddingsGallery::unknown_id) {
                            slog::info << "EmbeddingsGallery::unknown_id i=" << i << slog::endl;
                            break;
                        }
                        const auto& face = faces[i];
                        std::string label_to_draw = face_gallery.GetLabelByID(id);
                        // TODO: 점수 획득 필요(EmbeddingsGallery::m_distances 추가 필요)
                        //label_to_draw += " (" + std::to_string(face.confidence * 100) + ")";
                        sc_visualizer.DrawObject(face.rect, label_to_draw, red_color, white_color, true); // 얼굴 박스 그리기

                        // person box 에 face id 를 지정
                        getPedestrianTracker(0)->SetTrackFaceId(face.rect, id);
                    }

                    auto elapsed = std::chrono::high_resolution_clock::now() - started;
                    auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
                    work_time_ms += elapsed_ms;
#endif

#ifndef CU_DISABLE_ACTION_DETECTOR
                    for (size_t i = 0; i < faces.size(); i++) {
                        int label = ids.empty() ? EmbeddingsGallery::unknown_id : ids[i];
                        tracked_face_objects.emplace_back(faces[i].rect, faces[i].confidence, label);
                    }
                    tracker_reid.Process(prev_frame, tracked_face_objects, work_num_frames);

                    const auto tracked_faces = tracker_reid.TrackedDetectionsWithLabels();

                    TrackedObjects tracked_action_objects;
                    for (const auto& action : actions) { //skc actions 대신 person 만 리턴되면 될듯함
                        tracked_action_objects.emplace_back(action.rect, action.detection_conf, action.label);
                    }

                    tracker_action.Process(prev_frame, tracked_action_objects, work_num_frames);
                    const auto tracked_actions = tracker_action.TrackedDetectionsWithLabels();

                    auto elapsed = std::chrono::high_resolution_clock::now() - started;
                    auto elapsed_ms =
                        std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();

                    work_time_ms += elapsed_ms;

                    std::map<int, int> frame_face_obj_id_to_action;
                    for (size_t j = 0; j < tracked_faces.size(); j++) {
                        const auto& face = tracked_faces[j];
                        std::string label_to_draw;
                        if (face.label != EmbeddingsGallery::unknown_id)
                            label_to_draw += face_gallery.GetLabelByID(face.label);

                        int person_ind = GetIndexOfTheNearestPerson(face, tracked_actions);
                        int action_ind = default_action_index;
                        if (person_ind >= 0) {
                            action_ind = tracked_actions[person_ind].label;
                        }

                        if (actions_type == STUDENT) {
                            if (action_ind != default_action_index) {
                                label_to_draw += "[" + GetActionTextLabel(action_ind, actions_map) + "]";
                            }
                            frame_face_obj_id_to_action[face.object_id] = action_ind;
                            sc_visualizer.DrawObject(face.rect, label_to_draw, red_color, white_color, true); //skc 얼굴 박스 그리기(TODO: score 추가하자)
                            logger.AddFaceToFrame(face.rect, face_gallery.GetLabelByID(face.label), "");
                        }

                        if ((actions_type == TEACHER) && (person_ind >= 0)) {
                            if (face_gallery.GetLabelByID(face.label) == teacher_id) {
                                teacher_track_id = tracked_actions[person_ind].object_id;
                            }
                            else if (teacher_track_id == tracked_actions[person_ind].object_id) {
                                teacher_track_id = -1;
                            }
                        }
                    }

                    if (actions_type == STUDENT) {
                        for (const auto& action : tracked_actions) {
                            const auto& action_label = GetActionTextLabel(action.label, actions_map);
                            const auto& action_color = GetActionTextColor(action.label);
#if CU_NIPA
                            auto text_label = face_config.enabled ? "" : action_label;
                            for (size_t j = 0; j < tracked_faces.size(); j++) {
                                const auto& face = tracked_faces[j];
                                int person_ind = GetIndexOfTheNearestPerson(face, {action}); // 현재 action 박스에 face 가 포함됐는지 찾음
                                if (person_ind >= 0) {
                                    text_label = face_gallery.GetLabelByID(face.label);
                                    sc_visualizer.DrawObject(action.rect, text_label, action_color, white_color, true); //skc 사람 전체 박스 그리기
                                }
                            }
#else
                            const auto& text_label = face_config.enabled ? "" : action_label;
                            sc_visualizer.DrawObject(action.rect, text_label, action_color, white_color, true); //skc 사람 전체 박스 그리기
#endif
                            logger.AddPersonToFrame(action.rect, action_label, "");
                            logger.AddDetectionToFrame(action, work_num_frames);
                        }
                        face_obj_id_to_action_maps.push_back(frame_face_obj_id_to_action);
                    }
                    else if (teacher_track_id >= 0) {
                        auto res_find = std::find_if(tracked_actions.begin(), tracked_actions.end(),
                            [teacher_track_id](const TrackedObject& o) { return o.object_id == teacher_track_id; });
                        if (res_find != tracked_actions.end()) {
                            const auto& track_action = *res_find;
                            const auto& action_label = GetActionTextLabel(track_action.label, actions_map);
                            sc_visualizer.DrawObject(track_action.rect, action_label, red_color, white_color, true);
                            logger.AddPersonToFrame(track_action.rect, action_label, teacher_id);
                        }
                    }
#endif
                }

                sc_visualizer.DrawFPS(1e3f / (work_time_ms / static_cast<float>(work_num_frames) + 1e-6f),
                    red_color);

                ++work_num_frames;
            }

            ++total_num_frames;

            sc_visualizer.Show();

            if (FLAGS_last_frame >= 0 && work_num_frames > static_cast<size_t>(FLAGS_last_frame)) {
                break;
            }
            prev_frame = frame.clone();
            logger.FinalizeFrameRecord();
        }
        sc_visualizer.Finalize();

        slog::info << slog::endl;
        if (work_num_frames > 0) {
            const float mean_time_ms = work_time_ms / static_cast<float>(work_num_frames);
            slog::info << "Mean FPS: " << 1e3f / mean_time_ms << slog::endl;
        }
        slog::info << "Frames processed: " << total_num_frames << slog::endl;
        if (FLAGS_pc) {
            std::map<std::string, std::string>  mapDevices = getMapFullDevicesNames(ie, devices);
            face_detector.wait();
#ifndef CU_DISABLE_ACTION_DETECTOR
            action_detector.wait();
            action_detector.PrintPerformanceCounts(getFullDeviceName(mapDevices, FLAGS_d_act));
#endif
            face_detector.PrintPerformanceCounts(getFullDeviceName(mapDevices, FLAGS_d_fd));
            face_reid.PrintPerformanceCounts(getFullDeviceName(mapDevices, FLAGS_d_reid));
            landmarks_detector.PrintPerformanceCounts(getFullDeviceName(mapDevices, FLAGS_d_lm));
        }

#ifndef CU_DISABLE_ACTION_DETECTOR
        if (actions_type == STUDENT) {
            auto face_tracks = tracker_reid.vector_tracks();

            // correct labels for track
            std::vector<Track> new_face_tracks = UpdateTrackLabelsToBestAndFilterOutUnknowns(face_tracks);
            std::map<int, int> face_track_id_to_label = GetMapFaceTrackIdToLabel(new_face_tracks);

            if (reid_config.enabled && face_gallery.size() > 0) {
                std::map<int, FrameEventsTrack> face_obj_id_to_actions_track;
                ConvertActionMapsToFrameEventTracks(face_obj_id_to_action_maps, default_action_index,
                    &face_obj_id_to_actions_track);

                const int start_frame = 0;
                const int end_frame = face_obj_id_to_action_maps.size();
                std::map<int, RangeEventsTrack> face_obj_id_to_events;
                SmoothTracks(face_obj_id_to_actions_track, start_frame, end_frame,
                    smooth_window_size, smooth_min_length, default_action_index,
                    &face_obj_id_to_events);

                slog::info << "Final ID->events mapping" << slog::endl;
                logger.DumpTracks(face_obj_id_to_events,
                    actions_map, face_track_id_to_label,
                    face_gallery.GetIDToLabelMap());

                std::vector<std::map<int, int>> face_obj_id_to_smoothed_action_maps;
                ConvertRangeEventsTracksToActionMaps(end_frame, face_obj_id_to_events,
                    &face_obj_id_to_smoothed_action_maps);

                slog::info << "Final per-frame ID->action mapping" << slog::endl;
                logger.DumpDetections(cap.GetVideoPath(), frame.size(), work_num_frames,
                    new_face_tracks,
                    face_track_id_to_label,
                    actions_map, face_gallery.GetIDToLabelMap(),
                    face_obj_id_to_smoothed_action_maps);
            }
        }
#endif
    }
    catch (const std::exception& error) {
        slog::err << error.what() << slog::endl;
        return 1;
    }
    catch (...) {
        slog::err << "Unknown/internal exception happened." << slog::endl;
        return 1;
    }

    slog::info << "Execution successful" << slog::endl;

    return 0;
}