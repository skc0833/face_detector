# face_detector

## openvino(버전 2019.2.242) 에서 복사한 파일들
아래 사이트에서 openvino(버전 2019.2.242) 설치 후 필요한 header, library 를 복사해 독립 프로젝트로 구성함
https://software.seek.intel.com/openvino-toolkit?os=windows

현재 Submit 버튼 클릭시 The Information Provided is Incomplete or Invalid. 에러 발생중(19.8.5)
--> 회원가입시 이메일로 전달된 링크에서 다운로드 가능함. 

https://registrationcenter.intel.com/en/products/postregistration/?dnld=t&SN=C9PJ-F8VJPDH7&EmailID=skc0833@gmail.com&Sequence=2484580

풀패키지 설치 후, https://docs.openvinotoolkit.org/2019_R2/_docs_install_guides_installing_openvino_windows.html#set-the-environment-variables 
