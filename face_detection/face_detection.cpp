﻿/**
* \brief The entry point for the Inference Engine interactive_face_detection demo application
* \file interactive_face_detection_demo/main.cpp
* \example interactive_face_detection_demo/main.cpp
*/
#include "interactive_face_detection.hpp"
#include "detectors.hpp"
#include "face.hpp"
#include "visualizer.hpp"

// from C:\Program Files (x86)\IntelSWTools\openvino_2019.2.242\deployment_tools\open_model_zoo\demos\interactive_face_detection_demo\main.cpp
using namespace InferenceEngine;

bool ParseAndCheckCommandLine(int argc, char *argv[]) {
    // ---------------------------Parsing and validating input arguments--------------------------------------
    gflags::ParseCommandLineNonHelpFlags(&argc, &argv, true);
    if (FLAGS_h) {
        showUsage();
        showAvailableDevices();
        return false;
    }
    slog::info << "Parsing input parameters" << slog::endl;

    if (FLAGS_i.empty()) {
        throw std::logic_error("Parameter -i is not set");
    }

    if (FLAGS_m.empty()) {
        throw std::logic_error("Parameter -m is not set");
    }

    if (FLAGS_n_ag < 1) {
        throw std::logic_error("Parameter -n_ag cannot be 0");
    }

    if (FLAGS_n_hp < 1) {
        throw std::logic_error("Parameter -n_hp cannot be 0");
    }

    // no need to wait for a key press from a user if an output image/video file is not shown.
    FLAGS_no_wait |= FLAGS_no_show;

    return true;
}

int main(int argc, char *argv[]) {
    try {
        std::cout << "InferenceEngine: " << GetInferenceEngineVersion() << std::endl;

        // ------------------------------ Parsing and validating of input arguments --------------------------
        if (!ParseAndCheckCommandLine(argc, argv)) {
            return 0;
        }

        slog::info << "Reading input" << slog::endl;
        cv::VideoCapture cap;
        if (!(FLAGS_i == "cam" ? cap.open(0) : cap.open(FLAGS_i))) {
            throw std::logic_error("Cannot open input file or camera: " + FLAGS_i);
        }

        cv::Mat frame;
        if (!cap.read(frame)) {
            throw std::logic_error("Failed to get frame from cv::VideoCapture");
        }

        const size_t width = static_cast<size_t>(frame.cols);
        const size_t height = static_cast<size_t>(frame.rows);

        cv::VideoWriter videoWriter;
        if (!FLAGS_o.empty()) {
            videoWriter.open(FLAGS_o, cv::VideoWriter::fourcc('I', 'Y', 'U', 'V'), 25, cv::Size(width, height));
        }
        // ---------------------------------------------------------------------------------------------------
        // --------------------------- 1. Loading Inference Engine -----------------------------

        Core ie;

        std::set<std::string> loadedDevices;
        std::vector<std::pair<std::string, std::string>> cmdOptions = {
            {FLAGS_d, FLAGS_m},
        };
        FaceDetection faceDetector(FLAGS_m, FLAGS_d, 1, false, FLAGS_async, FLAGS_t, FLAGS_r,
            static_cast<float>(FLAGS_bb_enlarge_coef), static_cast<float>(FLAGS_dx_coef), static_cast<float>(FLAGS_dy_coef));

        for (auto && option : cmdOptions) {
            auto deviceName = option.first;
            auto networkName = option.second;

            if (deviceName.empty() || networkName.empty()) {
                continue;
            }

            if (loadedDevices.find(deviceName) != loadedDevices.end()) {
                continue;
            }
            slog::info << "Loading device " << deviceName << slog::endl;
            std::cout << ie.GetVersions(deviceName) << std::endl;

            /** Loading extensions for the CPU device **/
            if ((deviceName.find("CPU") != std::string::npos)) {
                ie.AddExtension(std::make_shared<Extensions::Cpu::CpuExtensions>(), "CPU");

                if (!FLAGS_l.empty()) {
                    // CPU(MKLDNN) extensions are loaded as a shared library and passed as a pointer to base extension
                    // [190906][skc] 프로젝트 속성에 일반 / 문자 집합을 "멀티바이트 문자 집합 사용" 으로 설정하지 않으면(default "유니코드 문자 집합 사용") 아래 컴파일 에러 발생중
                    // error C2664: 'std::shared_ptr<InferenceEngine::IExtension> InferenceEngine::make_so_pointer<InferenceEngine::IExtension>(const file_name_t &)': 
                    // 인수 1을(를) 'fLS::clstring'에서 'const file_name_t &'(으)로 변환할 수 없습니다.
                    auto extension_ptr = make_so_pointer<IExtension>(FLAGS_l);
                    ie.AddExtension(extension_ptr, "CPU");
                    slog::info << "CPU Extension loaded: " << FLAGS_l << slog::endl;
                }
            }
            else if (!FLAGS_c.empty()) {
                // Loading extensions for GPU
                ie.SetConfig({ {PluginConfigParams::KEY_CONFIG_FILE, FLAGS_c} }, "GPU");
            }

            loadedDevices.insert(deviceName);
        }

        /** Per-layer metrics **/
        if (FLAGS_pc) {
            ie.SetConfig({ {PluginConfigParams::KEY_PERF_COUNT, PluginConfigParams::YES} });
        }
        // ---------------------------------------------------------------------------------------------------

        // --------------------------- 2. Reading IR models and loading them to plugins ----------------------
        // Disable dynamic batching for face detector as it processes one image at a time
        Load(faceDetector).into(ie, FLAGS_d, false);
        // ----------------------------------------------------------------------------------------------------

        // --------------------------- 3. Doing inference -----------------------------------------------------
        // Starting inference & calculating performance
        slog::info << "Start inference " << slog::endl;

        std::ostringstream out;
        size_t framesCounter = 0;
        bool frameReadStatus;
        bool isLastFrame;
        int delay = 1;
        cv::Mat prev_frame, next_frame;
        std::list<Face::Ptr> faces;
        size_t id = 0;

        Visualizer::Ptr visualizer;
        visualizer = std::make_shared<Visualizer>(cv::Size(width, height));

        // Detecting all faces on the first frame and reading the next one
        faceDetector.enqueue(frame);
        faceDetector.submitRequest();

        prev_frame = frame.clone();

        // Reading the next frame
        frameReadStatus = cap.read(frame);

        std::cout << "To close the application, press 'CTRL+C' here";
        if (!FLAGS_no_show) {
            std::cout << " or switch to the output window and press any key";
        }
        std::cout << std::endl;

        bool check_face_detect_time = true;
        Timer timer;
        while (true) {
            timer.start("total");
            framesCounter++;
            isLastFrame = !frameReadStatus;

            if (check_face_detect_time) timer.start("face_detect");
            // Retrieving face detection results for the previous frame
            faceDetector.wait();
            faceDetector.fetchResults();
            auto prev_detection_results = faceDetector.results;

            // No valid frame to infer if previous frame is the last
            if (!isLastFrame) {
                faceDetector.enqueue(frame);
                faceDetector.submitRequest();
            }
            if (check_face_detect_time) {
                timer.finish("face_detect"); // debug 100~120ms, release 20~30ms
                slog::info << "face detect time: " << std::fixed << std::setprecision(2) << timer["face_detect"].getLastCallDuration() << " ms" << slog::endl;
            }

            // Reading the next frame if the current one is not the last
            if (!isLastFrame) {
                frameReadStatus = cap.read(next_frame);
                if (FLAGS_loop_video && !frameReadStatus) {
                    if (!(FLAGS_i == "cam" ? cap.open(0) : cap.open(FLAGS_i))) {
                        throw std::logic_error("Cannot open input file or camera: " + FLAGS_i);
                    }
                    frameReadStatus = cap.read(next_frame);
                }
            }

            //  Postprocessing
            std::list<Face::Ptr> prev_faces;

            if (!FLAGS_no_smooth) {
                prev_faces.insert(prev_faces.begin(), faces.begin(), faces.end());
            }

            faces.clear();

            // For every detected face
            for (size_t i = 0; i < prev_detection_results.size(); i++) {
                auto& result = prev_detection_results[i];
                cv::Rect rect = result.location & cv::Rect(0, 0, width, height);

                Face::Ptr face;
                face = std::make_shared<Face>(id++, rect);

                faces.push_back(face);
            }

            //  Visualizing results
            if (!FLAGS_no_show || !FLAGS_o.empty()) {
                // drawing faces
                visualizer->draw(prev_frame, faces);

                // show fps
                {
                    out.str("");
                    out << "Total image throughput: " << std::fixed << std::setprecision(2) << 1000.f / (timer["total"].getSmoothedDuration()) << " fps";
                    //out << "Total image throughput: " << std::fixed << std::setprecision(2) << 1000.f / (timer["total"].getLastCallDuration()) << " fps";
                    cv::putText(prev_frame, out.str(), cv::Point2f(10, 45), cv::FONT_HERSHEY_TRIPLEX, 1.2,
                        cv::Scalar(255, 0, 0), 2);
                }

                if (!FLAGS_no_show) {
                    cv::imshow("Detection results", prev_frame);
                }
            }

            if (!FLAGS_o.empty()) {
                videoWriter.write(prev_frame);
            }

            prev_frame = frame;
            frame = next_frame;
            next_frame = cv::Mat();

            timer.finish("total");

            // End of file (or a single frame file like an image). The last frame is displayed to let you check what is shown
            if (isLastFrame) {
                if (!FLAGS_no_wait) {
                    std::cout << "No more frames to process!" << std::endl;
                    cv::waitKey(0);
                }
                break;
            }
            else if (!FLAGS_no_show && -1 != cv::waitKey(delay)) {
                break;
            }
        }

        slog::info << "Number of processed frames: " << framesCounter << slog::endl;
        slog::info << "Total image throughput: " << framesCounter * (1000.f / timer["total"].getTotalDuration()) << " fps" << slog::endl;

        if (check_face_detect_time) {
            // debug 140ms, release 25ms
            slog::info << slog::endl << "Everage face detect time: " << std::fixed << std::setprecision(2)
                << timer["face_detect"].getTotalDuration() / framesCounter << " ms" << slog::endl << slog::endl;
        }

        // Showing performance results
        if (FLAGS_pc) {
            faceDetector.printPerformanceCounts(getFullDeviceName(ie, FLAGS_d));
        }
        // ---------------------------------------------------------------------------------------------------

        if (!FLAGS_o.empty()) {
            videoWriter.release();
        }

        // release input video stream
        cap.release();

        // close windows
        cv::destroyAllWindows();
    }
    catch (const std::exception& error) {
        slog::err << error.what() << slog::endl;
        return 1;
    }
    catch (...) {
        slog::err << "Unknown/internal exception happened." << slog::endl;
        return 1;
    }

    slog::info << "Execution successful" << slog::endl;
    return 0;
}
