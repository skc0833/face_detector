// Copyright (C) 2018-2019 Intel Corporation
// SPDX-License-Identifier: Apache-2.0
//

# pragma once
#include <string>
#include <map>
#include <memory>
#include <utility>
#include <list>
#include <vector>
#include <opencv2/opencv.hpp>

#include "detectors.hpp"

// -------------------------Describe detected face on a frame-------------------------------------------------

struct Face {
public:
    using Ptr = std::shared_ptr<Face>;

    explicit Face(size_t id, cv::Rect& location);

    void updateLandmarks(std::vector<float> values);

    const std::vector<float>& getLandmarks();
    size_t getId();

    void landmarksEnable(bool value);
    bool isLandmarksEnabled();

public:
    cv::Rect _location;
    float _intensity_mean;

private:
    size_t _id;
    std::vector<float> _landmarks;
    bool _isLandmarksEnabled;
};

// ----------------------------------- Utils -----------------------------------------------------------------
float calcIoU(cv::Rect& src, cv::Rect& dst);
float calcMean(const cv::Mat& src);
Face::Ptr matchFace(cv::Rect rect, std::list<Face::Ptr>& faces);
