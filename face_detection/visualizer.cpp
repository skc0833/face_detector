// Copyright (C) 2018-2019 Intel Corporation
// SPDX-License-Identifier: Apache-2.0
//

#include <memory>
#include <string>
#include <list>
#include <vector>
#include <map>
#include <algorithm>

#include "visualizer.hpp"

// PhotoFrameVisualizer
PhotoFrameVisualizer::PhotoFrameVisualizer(int bbThickness, int photoFrameThickness, float photoFrameLength) :
    bbThickness(bbThickness), photoFrameThickness(photoFrameThickness), photoFrameLength(photoFrameLength) {
}

void PhotoFrameVisualizer::draw(cv::Mat& img, cv::Rect& bb, cv::Scalar color) {
    cv::rectangle(img, bb, color, bbThickness);

    auto drawPhotoFrameCorner = [&](cv::Point p, int dx, int dy) {
        cv::line(img, p, cv::Point(p.x, p.y + dy), color, photoFrameThickness);
        cv::line(img, p, cv::Point(p.x + dx, p.y), color, photoFrameThickness);
    };

    int dx = static_cast<int>(photoFrameLength * bb.width);
    int dy = static_cast<int>(photoFrameLength * bb.height);

    drawPhotoFrameCorner(bb.tl(), dx, dy);
    drawPhotoFrameCorner(cv::Point(bb.x + bb.width - 1, bb.y), -dx, dy);
    drawPhotoFrameCorner(cv::Point(bb.x, bb.y + bb.height - 1), dx, -dy);
    drawPhotoFrameCorner(cv::Point(bb.x + bb.width - 1, bb.y + bb.height - 1), -dx, -dy);
}

// Visualizer
Visualizer::Visualizer(cv::Size const& imgSize, int leftPadding, int rightPadding, int topPadding, int bottomPadding) :
    photoFrameVisualizer(std::make_shared<PhotoFrameVisualizer>()) {}

void Visualizer::drawFace(cv::Mat& img, Face::Ptr f, bool drawEmotionBar) {
    photoFrameVisualizer->draw(img, f->_location, cv::Scalar(0, 255, 0));
}
void Visualizer::draw(cv::Mat img, std::list<Face::Ptr> faces) {
    for (auto&& face : faces) {
        drawFace(img, face, false);
    }
}
